<?php

use App\User;
use App\Module;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now =  Carbon::now();
        $user = User::create([
            'email' => 'admin@htm.huatan.com.ph',
            'password' => Hash::make('11111111'),
            'role' => User::ROLE_SUPER_ADMIN,
            'status' => User::STATUS_ACTIVE,
            'created_at' => $now,
            'updated_at' => $now
        ]);

        $user->meta()->createMany([
            ['name' => 'FirstName', 'key' => 'firstname', 'value' =>  'Admin'],
            ['name' => 'LastName', 'key' => 'lastname', 'value' =>  '']
        ]);

        $modules = Module::all();
        $actions = Module::$actions;

        foreach($modules as $module) {
            foreach($actions as $action) {
                $user->modules()->attach($module->id, ['action' => $action]);
            }
        }        
    }
}

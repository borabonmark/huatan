<?php

use App\Module;
use Illuminate\Database\Seeder;

class ModulesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Module::insert([
        	['name' => 'Merchant'],
        	['name' => 'Advertisement'],
        	['name' => 'User'],
            ['name' => 'Article']
        ]);
    }
}

<?php

use App\Category;
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::insert([
            ['type' => Category::TYPE_BUSINESS, 'name' => 'Hotel'],
            ['type' => Category::TYPE_BUSINESS, 'name' => 'Restaurant'],
            ['type' => Category::TYPE_BUSINESS, 'name' => 'Club'],
            ['type' => Category::TYPE_NEWS, 'name' => 'Politics'],
            ['type' => Category::TYPE_NEWS, 'name' => 'Sports'],
            ['type' => Category::TYPE_NEWS, 'name' => 'Technology'],
            ['type' => Category::TYPE_NEWS, 'name' => 'Business'],
            ['type' => Category::TYPE_NEWS, 'name' => 'Entertainment'],
            ['type' => Category::TYPE_NEWS, 'name' => 'Lifestyle']
        ]);
    }
}

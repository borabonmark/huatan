<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(
[
	'prefix' => LaravelLocalization::setLocale(),
    'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]
],
function(){
	Auth::routes();
});

Route::middleware(['auth'])->group(function () {
	Route::group(
	[
		'prefix' => LaravelLocalization::setLocale(),
	    'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]
	],
	function(){
		Route::get('/', 'HomeController@index')->name('home');

		// Advertisements
		Route::get('/advertisements/list', 'AdvertisementController@list')->name('advertisements.list');
		Route::get('/advertisements/list/get', 'AdvertisementController@getList')->name('advertisements.getList');
		Route::resource('advertisements', 'AdvertisementController');
		Route::post('/advertisements/status/update', 'AdvertisementController@statusUpdate')->name('advertisements.statusUpdate');

		// Merchants
		Route::get('/merchants/list', 'MerchantController@list')->name('merchants.list');
		Route::get('/merchants/list/get', 'MerchantController@getList')->name('merchants.getList');
		Route::resource('merchants', 'MerchantController');
		Route::post('/merchants/status/update', 'MerchantController@statusUpdate')->name('merchants.statusUpdate');

		// Articles
		Route::get('/articles/list', 'ArticleController@list')->name('articles.list');
		Route::get('/articles/list/get', 'ArticleController@getList')->name('articles.getList');
		Route::resource('articles', 'ArticleController');
		Route::post('/articles/status/update', 'ArticleController@statusUpdate')->name('articles.statusUpdate');

		// Users
		Route::get('/users/list/{role?}', 'UserController@list')->name('users.list');
		Route::get('/users/list/get/{role?}', 'UserController@getList')->name('users.getList');
		Route::resource('users', 'UserController');
		Route::post('/users/status/update', 'UserController@statusUpdate')->name('users.statusUpdate');
		Route::post('/users/{user}/change-password', 'UserController@changePassword')->name('users.changepassword');
		Route::post('/users/{user}/modules', 'UserController@assignModules')->name('users.modules');

		// Account settings
		Route::get('/account/settings', 'AccountController@settings')->name('account.settings');
		Route::patch('/account/settings', 'AccountController@update')->name('account.update');
		Route::post('/account/change-password', 'AccountController@changePassword')->name('account.changepassword');

		// Comments
		Route::resource('comments', 'CommentController');

		// Images
		Route::post('/images/upload', 'ImageController@upload')->name('images.upload');

		// Tags
		Route::get('/tags/search', 'TagController@search')->name('tags.search');
	});
});

Route::get('/articlew/{article}', 'ArticleController@webView');
<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get('/', function () {
    return view('api/index');
});


// ADVERTISEMENT
Route::get('/advertisements', 'api\AdvertisementController@index');
Route::get('/advertisement/{status}', 'api\AdvertisementController@status');

// ARTICLES
Route::get('/articles', 'api\ArticleController@index');
Route::get('/article/{article}', 'api\ArticleController@show');
Route::get('/article/paginateby/{count}', 'api\ArticleController@paginateby');
Route::get('/article/bycategory/{category_id}', 'api\ArticleController@bycategory');
Route::post('/article/click/{id}', 'api\ArticleController@click');

// CATEGORIES
Route::get('/categories', 'api\CategoryController@index');
Route::get('/category/{type}', 'api\CategoryController@bytype');

// MERCHANT
Route::get('/merchants', 'api\MerchantController@index');
Route::get('/merchants/priority', 'api\MerchantController@priority');

// USER REGISTRATION
Route::post('/register', 'api\RegistrationController@verify_number');
// Route::get('/sendemail/{code}', 'api\RegistrationController@sendemail');

// RESET PASSWORD
Route::get('/resetpassword/{phone}', 'api\RegistrationController@reset_password');

//COMMENT
Route::get('/comments', 'api\CommentController@index');
Route::get('/comments/{post_id}/{type}', 'api\CommentController@show');
Route::get('/comments/{post_id}/{comment_id}/{type}', 'api\CommentController@reply');

Route::get('/user/{id}', 'api\UserDetailController@show');

// FORUM
Route::get('/forums', 'api\ForumController@index');
Route::get('/forums/{city}', 'api\ForumController@show');
Route::get('/forums/user/{id}', 'api\ForumController@byuser');
Route::get('/forums/post/today', 'api\ForumController@bytoday');
Route::get('/forums/user/{id}/count', 'api\ForumController@countbyuser');
Route::get('/forums/post/today/count', 'api\ForumController@countbytoday');

Route::middleware(['auth:api'])->group(function () {

	// BOOKMARK
	Route::post('/bookmark/store', 'api\BookmarkController@store');
	Route::delete('/bookmark/delete/{id}/{type}', 'api\BookmarkController@destroy');
	Route::get('/bookmark/check/{id}/{type}', 'api\BookmarkController@check');
	Route::get('/bookmark/merchant', 'api\BookmarkController@show_merchant');
	Route::get('/bookmark/article', 'api\BookmarkController@show_article');
	Route::get('/bookmark/user', 'api\BookmarkController@show_user');
	Route::get('/bookmark/post', 'api\BookmarkController@show_post');

	// USER DETAILS
	Route::post('/user/changepassword', 'api\RegistrationController@change_password');
	Route::get('/user', 'api\UserDetailController@index');
	Route::post('/user/update', 'api\UserDetailController@store');

	// UsER STATUS
	Route::get('/user/status/online', 'api\UserDetailController@online');

	// USER IMAGE
	Route::post('/user/uploadimage', 'api\UserImageController@store');
	Route::delete('/user/uploadimage/delete/{id}', 'api\UserImageController@destroy');
	Route::post('/user/updateavatar', 'api\UserDetailController@storeavatar');

	// GEO LOCATION
	Route::post('/user/geolocation/update', 'api\UserDetailController@geolocation');
	Route::get('/user/geolocation/all', 'api\UserGeoController@index');

	// USER VERIFICATION
	Route::get('/register/verify/{code}', 'api\RegistrationController@smsverify');
	Route::get('/register/resend', 'api\RegistrationController@resendsms');

	//COMMENT
	Route::post('/comment/store', 'api\CommentController@store');
	Route::post('/comment/reply/store', 'api\CommentController@storereply');
	Route::post('/comment/filestore', 'api\CommentController@filestore');
	Route::post('/comment/reply/filestore', 'api\CommentController@filestorereply');
	Route::put('/comment/update/{id}', 'api\CommentController@update');
	Route::delete('/comment/delete/{id}', 'api\CommentController@destroy');

	// FORUM
	Route::post('/forum/store', 'api\ForumController@store');
	Route::delete('/forum/delete/{id}', 'api\ForumController@destroy');
	Route::put('/forum/update/{id}', 'api\ForumController@update');

	// REPORT
	Route::post('/report/store', 'api\ReportController@store');

});


// Route::apiResource('/article', 'api\ArticleController');
// Route::group(['prefix'=>'post'],function(){
// 	Route::apiResource('/{post}/comments','CommentController');
// });
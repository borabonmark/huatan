<?php
return [
    "dictDefaultMessage" => "Drop files here to upload",
    "dictFallbackMessage" => "Your browser does not support drag'n'drop file uploads.",
    "dictFallbackText" => "Please use the fallback form below to upload your files like in the olden days.",
    "dictFileTooBig" => "File is too big ",
    "dictInvalidFileType" => "You can't upload files of this type.",
    "dictResponseError" => "Server error",
    "dictCancelUpload" => "Cancel upload",
    "dictUploadCanceled" => "Upload canceled.",
    "dictCancelUploadConfirmation" => "Are you sure you want to cancel this upload?",
    "dictRemoveFile" => "Remove file",
    "dictMaxFilesExceeded" => "You can not upload any more files."
];
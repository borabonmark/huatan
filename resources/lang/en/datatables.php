<?php
return [
    'Show' => 'Show',
    'entries' => 'entries',
    'Search' => 'Search',
    'Showing' => 'Showing',
    'Previous' => 'Previous',
    'Next' => 'Next',
    'of' => 'of',
    'to' => 'to',
    'First' => 'First',
    'Last' => 'Last',
    'no_data' => 'No data available in table',
    'zero_entries' => 'Showing 0 to 0 of 0 entries',
    'loading' => 'Loading...',
    'processing' => 'Processing...',
    'no_match' => 'No matching records found',
    'filter_from' => 'filtered from',
    'total_entries' => 'total entries',
    'activate_sort_asc' => 'activate to sort column ascending',
    'activate_sort_desc' => 'activate to sort column descending'
];
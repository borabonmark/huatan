<?php
return [
    'dictDefaultMessage' => '在这里放下文件上载',
    'dictFallbackMessage' => '您的浏览器不支持拖放文件上传。',
    'dictFallbackText' => '请使用下面的备份表格来上传你以前的文件。',
    'dictFileTooBig' => '文件太大了。',
    'dictInvalidFileType' => '您不能上传这种类型的文件。',
    'dictResponseError' => '服务器错误',
    'dictCancelUpload' => '取消上传',
    'dictUploadCanceled' => 'U取消上传',
    'dictCancelUploadConfirmation' => '你确定要取消这个上传吗?',
    'dictRemoveFile' => '删除文件',
    'dictMaxFilesExceeded' => '你不能再上传任何文件了。'
];
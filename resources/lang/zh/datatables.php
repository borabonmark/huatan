<?php
return [
    'Show' => '显示',
    'entries' => '条目',
    'Search' => '搜索',
    'Showing' => '显示',
    'Previous' => '以前的',
    'Next' => '下一个',
    'of' => '的',
    'to' => '来',
    'First' => '第一个',
    'Last' => '去年',
    'no_data' => '表中没有可用的数据',
    'zero_entries' => '显示0到0个条目',
    'loading' => '加载……',
    'processing' => '...加工',
    'no_match' => '没有找到匹配的记录',
    'filter_from' => '过滤器从',
    'total_entries' => '总条目',
    'activate_sort_asc' => '激活排序列升序',
    'activate_sort_desc' => '激活排序降序列'
];
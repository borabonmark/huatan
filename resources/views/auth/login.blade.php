@extends('layouts.auth')

@section('content')
<div class="vertical-align-wrap">
    <div class="vertical-align-middle">
        <div class="auth-box ">
            <div class="left">
                <div class="content">
                    <div class="header">
                        <div class="logo text-center"><img src="{{ asset('img/logo.png') }}" alt="{{ config('app.name', 'HT Manage') }}"></div>
                        <p class="lead">{{ __('Login to your account') }}</p>
                    </div>
                    <form method="POST" action="{{ route('login') }}" class="form-auth-small">
                        @csrf

                        <div class="form-group  {{ $errors->has('email') ? 'has-error has-feedback' : '' }}">
                            <label for="email" class="control-label sr-only">{{ __('Email') }}</label>
                            <input type="email" name="email" id="email" class="form-control" value="{{ old('email') }}" placeholder="{{ __('Email') }}" required>

                            @if ($errors->has('email'))
                                <span class="help-block text-left">{{ $errors->first('email') }}</span>
                            @endif
                        </div>
                        <div class="form-group {{ $errors->has('password') ? 'has-error has-feedback' : '' }}">
                            <label for="password" class="control-label sr-only">{{ __('Password') }}</label>
                            <input type="password" name="password" id="password" class="form-control" placeholder="{{ __('Password') }}" required>

                            @if ($errors->has('password'))
                                <span class="help-block text-left">{{ $errors->first('password') }}</span>
                            @endif
                        </div>
                        <button type="submit" class="btn btn-primary btn-lg btn-block">{{ __('Login') }}</button>
                        <div class="bottom">
                            <span class="helper-text"><i class="fa fa-lock"></i> 
                                <a href="{{ route('password.request') }}">{{ __('Forgot password') }}?</a>
                            </span>
                        </div>
                    </form>
                </div>
            </div>
            <div class="right">
                <div class="overlay"></div>
                <div class="content text">
                    <h1 class="heading">{{ __('Welcome to HT Manage') }}</h1>
                    <p>{{ __('by TSSI') }}</p>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
@endsection

@extends('layouts.auth')

@section('content')
<div class="vertical-align-wrap">
    <div class="vertical-align-middle">
        <div class="auth-box ">
            <div class="left">
                <div class="content">
                    <div class="header">
                        <div class="logo text-center"><img src="{{ asset('img/logo.png') }}" alt="{{ config('app.name', 'HT Manage') }}"></div>
                        <p class="lead">{{ __('Reset Password') }}</p>

                        @if (session('status'))
                            @component('alert', ['type' => 'success'])
                                {!! session('status') !!}
                            @endcomponent
                        @endif
                    </div>
                    <form method="POST" action="{{ route('password.email') }}" class="form-auth-small">
                        @csrf

                        <div class="form-group  {{ $errors->has('email') ? 'has-error has-feedback' : '' }}">
                            <label for="email" class="control-label sr-only">{{ __('Email') }}</label>
                            <input type="email" name="email" id="email" class="form-control" value="{{ old('email') }}" placeholder="{{ __('Email') }}" required>

                            @if ($errors->has('email'))
                                <span class="help-block text-left">{{ $errors->first('email') }}</span>
                            @endif
                        </div>
                        
                        <button type="submit" class="btn btn-primary btn-lg btn-block">{{ __('Send Password Reset Link') }}</button>
                        <div class="bottom">
                            <span class="helper-text"><i class="fa fa-sign-in"></i> 
                                <a href="{{ route('login') }}">{{ __('Login') }}</a>
                            </span>
                        </div>
                    </form>
                </div>
            </div>
            <div class="right resetpassword">
                <div class="overlay"></div>
                <div class="content text">
                    <h1 class="heading">{{ __('Reset Password') }}</h1>
                    <p>{{ __('Enter your email address and we will send you the password reset link.') }}</p>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
@endsection

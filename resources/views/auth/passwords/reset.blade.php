@extends('layouts.auth')

@section('content')
<div class="vertical-align-wrap">
    <div class="vertical-align-middle">
        <div class="auth-box ">
            <div class="left">
                <div class="content">
                    <div class="header">
                        <div class="logo text-center"><img src="{{ asset('img/logo.png') }}" alt="{{ config('app.name', 'HT Manage') }}"></div>
                        <p class="lead">{{ __('Reset Password') }}</p>

                        @if (session('status'))
                            @component('alert', ['type' => 'success'])
                                {!! session('status') !!}
                            @endcomponent
                        @endif
                    </div>
                    <form method="POST" action="{{ route('password.request') }}" class="form-auth-small">
                        @csrf

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="form-group  {{ $errors->has('email') ? 'has-error has-feedback' : '' }}">
                            <label for="email" class="control-label sr-only">{{ __('Email') }}</label>
                            <input type="email" name="email" id="email" class="form-control" value="{{ old('email') }}" placeholder="{{ __('Email') }}" required>

                            @if ($errors->has('email'))
                                <span class="help-block text-left">{{ $errors->first('email') }}</span>
                            @endif
                        </div>

                        <div class="form-group {{ $errors->has('password') ? 'has-error has-feedback' : '' }}">
                            <label for="password" class="control-label sr-only">{{ __('New Password') }}</label>
                            <input type="password" name="password" id="password" class="form-control" placeholder="{{ __('New Password') }}" required>

                            @if ($errors->has('password'))
                                <span class="help-block text-left">{{ $errors->first('password') }}</span>
                            @endif
                        </div>

                        <div class="form-group {{ $errors->has('password_confirmation') ? 'has-error has-feedback' : '' }}">
                            <label for="password_confirmation" class="control-label sr-only">{{ __('Confirm Password') }}</label>
                            <input type="password" name="password_confirmation" id="password_confirmation" class="form-control" placeholder="{{ __('Confirm New Password') }}" required>

                            @if ($errors->has('password_confirmation'))
                                <span class="help-block text-left">{{ $errors->first('password_confirmation') }}</span>
                            @endif
                        </div>
                        
                        <button type="submit" class="btn btn-primary btn-lg btn-block">{{ __('Reset Password') }}</button>
                        <div class="bottom">
                            <span class="helper-text"><i class="fa fa-sign-in"></i>
                                <a href="{{ route('login') }}">{{ __('Login') }}</a>
                            </span>
                        </div>
                    </form>
                </div>
            </div>
            <div class="right resetpassword">
                <div class="overlay"></div>
                <div class="content text">
                    <h1 class="heading">{{ __('Reset Password') }}</h1>
                    <p>{{ __('Enter your new password.') }}</p>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
@endsection
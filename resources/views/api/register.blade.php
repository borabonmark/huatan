<div class="card">
    <div class="card-header" id="headingThree">
      <h5 class="mb-0">
        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseFour">
          Register Route
        </button>
      </h5>
    </div>
    <div id="collapseSeven" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
      <div class="card-body">
        <div class="col align-self-center">
     		<table class="table table-hover">
			  <thead>
			    <tr>		      
			      <th scope="col">Request type</th>
			      <th scope="col">link</th>		    
			    </tr>
			  </thead>
			  <tbody>			
			    <tr>		  
			      <td>
			      	<h5><span class="badge badge-primary">Get</span></h5>
			      </td>
			      <td>
			      	<code>api/resetpassword/{phone}</code>
			    	<p>Resend sms verification code</p>
			      </td>		    
			    </tr>
			    <tr>		  
			      <td>
			      	<h5><span class="badge badge-primary">Get</span></h5>
			      </td>
			      <td>
			      	<code>api/register/verify/{smsCode}</code>
			    	<p>Verify user phone number | required login</p>
			      </td>		    
			    </tr>
			    <tr>		  
			      <td>
			      	<h5><span class="badge badge-success">POST</span></h5>
			      </td>
			      <td>
			      	<code>api/register</code>
			      	<p>Register</p>
			      	<code>phone:text|required</code><br>
			      	<code>password:password|required</code>
			    	<p>Fields</p>
			      </td>		    
			    </tr>
			    <tr>		  
			      <td>
			      	<h5><span class="badge badge-success">POST</span></h5>
			      </td>
			      <td>
			      	<code>oauth/token</code>
			    	<p>Request a token</p>
			      </td>		    
			    </tr>		    
			  </tbody>
			</table>
     	</div>
      </div>
    </div>
  </div>
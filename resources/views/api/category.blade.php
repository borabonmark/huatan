<div class="card">
    <div class="card-header" id="headingTwo">
      <h5 class="mb-0">
        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
          Category Route
        </button>
      </h5>
    </div>
    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
      <div class="card-body">
        <div class="col align-self-center">
     		<table class="table table-hover">
			  <thead>
			    <tr>		      
			      <th scope="col">Request type</th>
			      <th scope="col">link</th>		    
			    </tr>
			  </thead>
			  <tbody>
			    <tr>		  
			      <td>
			      	<h5><span class="badge badge-primary">Get</span></h5>
			      </td>
			      <td>
			      	<code>api/categories</code>
			    	<p>Get all categories</p>
			      </td>		    
			    </tr>
			    <tr>		  
			      <td>
			      	<h5><span class="badge badge-primary">Get</span></h5>
			      </td>
			      <td>
			      	<code>api/category/{type}</code>
			    	<p>Get specific type of category</p>
			      </td>		    
			    </tr>		    
			  </tbody>
			</table>
     	</div>
      </div>
    </div>
  </div>
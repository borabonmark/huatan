<div class="card">
    <div class="card-header" id="headingOne">
      <h5 class="mb-0">
        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
          Article Route
        </button>
      </h5>
    </div>
    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
      <div class="card-body">
     	<div class="col align-self-center">
     		<table class="table table-hover">
			  <thead>
			    <tr>		      
			      <th scope="col">Request type</th>
			      <th scope="col">link</th>		    
			    </tr>
			  </thead>
			  <tbody>
			    <tr>		  
			      <td>
			      	<h5><span class="badge badge-primary">Get</span></h5>
			      </td>
			      <td>
			      	<code>api/articles</code>
			    	<p>Get all articles</p>
			      </td>		    
			    </tr>
			    <tr>		  
			      <td>
			      	<h5><span class="badge badge-primary">Get</span></h5>
			      </td>
			      <td>
			      	<code>api/article/{articleId}</code>
			    	<p>Get specific article</p>
			      </td>		    
			    </tr>
			    <tr>		  
			      <td>
			      	<h5><span class="badge badge-primary">Get</span></h5>
			      </td>
			      <td>
			      	<code>api/article/paginateby/{count}</code>
			    	<p>Get all articles with paginatetion</p>
			      </td>		    
			    </tr>
			    <tr>		  
			      <td>
			      	<h5><span class="badge badge-primary">Get</span></h5>
			      </td>
			      <td>
			      	<code>api/article/bycategory/{category_id}</code>
			    	<p>Get articles by category</p>
			      </td>		    
			    </tr>
			    <tr>		  
			      <td>
			      	<h5><span class="badge badge-success">POST</span></h5>
			      </td>
			      <td>
			      	<code>api/article/click</code>
			      	<p>Triggers when post is click</p>
			      </td>		    
			    </tr>			    
			  </tbody>
			</table>
     	</div>
      </div>
    </div>
  </div>
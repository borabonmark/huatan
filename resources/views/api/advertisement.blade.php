<div class="card">
    <div class="card-header" id="headingThree">
      <h5 class="mb-0">
        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
          Advertisement Route
        </button>
      </h5>
    </div>
    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
      <div class="card-body">
        <div class="col align-self-center">
     		<table class="table table-hover">
			  <thead>
			    <tr>		      
			      <th scope="col">Request type</th>
			      <th scope="col">link</th>		    
			    </tr>
			  </thead>
			  <tbody>
			    <tr>		  
			      <td>
			      	<h5><span class="badge badge-primary">Get</span></h5>
			      </td>
			      <td>
			      	<code>api/advertisements</code>
			    	<p>Get all advertisements</p>
			      </td>		    
			    </tr>
			    <tr>		  
			      <td>
			      	<h5><span class="badge badge-primary">Get</span></h5>
			      </td>
			      <td>
			      	<code>api/advertisement/{status}</code>
			    	<p>Get offline/online status advertisements</p>
			      </td>		    
			    </tr>		    
			  </tbody>
			</table>
     	</div>
      </div>
    </div>
  </div>
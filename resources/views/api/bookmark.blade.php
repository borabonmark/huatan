<div class="card">
    <div class="card-header" id="headingEight">
      <h5 class="mb-0">
        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
          Bookmark Route
        </button>
      </h5>
    </div>
    <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion">
      <div class="card-body">
     	<div class="col align-self-center">
     		<table class="table table-hover">
			  <thead>
			    <tr>		      
			      <th scope="col">Request type</th>
			      <th scope="col">link</th>		    
			    </tr>
			  </thead>
			  <tbody>
			    <tr>		  
			      <td>
			      	<h5><span class="badge badge-primary">Get</span></h5>
			      </td>
			      <td>
			      	<code>api/bookmark/article</code>
			    	<p>Get all bookmarked article | required login</p>
			      </td>		    
			    </tr>
			    <tr>		  
			      <td>
			      	<h5><span class="badge badge-primary">Get</span></h5>
			      </td>
			      <td>
			      	<code>api/bookmark/merchant</code>
			    	<p>Get all bookmarked merchant | required login</p>
			      </td>		    
			    </tr>
			    <tr>		  
			      <td>
			      	<h5><span class="badge badge-primary">Get</span></h5>
			      </td>
			      <td>
			      	<code>api/bookmark/check</code>
			    	<p>Check if item is bookmarked | required login</p>
			      </td>		    
			    </tr>
			    <tr>		  
			      <td>
			      	<h5><span class="badge badge-success">Post</span></h5>
			      </td>
			      <td>
			      	<code>api/bookmark/store</code>
			    	<p>Bookmark item | required login</p>
			      </td>		    
			    </tr>
			    <tr>		  
			      <td>
			      	<h5><span class="badge badge-danger">Delete</span></h5>
			      </td>
			      <td>
			      	<code>api/bookmark/delete</code>
			    	<p> Unbookmark item | required login</p>
			      </td>		    
			    </tr>

			    			    
			  </tbody>
			</table>
     	</div>
      </div>
    </div>
  </div>
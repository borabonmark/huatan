<div class="card">
    <div class="card-header" id="headingThree">
      <h5 class="mb-0">
        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseFour">
          Comment Route
        </button>
      </h5>
    </div>
    <div id="collapseSix" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
      <div class="card-body">
        <div class="col align-self-center">
     		<table class="table table-hover">
			  <thead>   
			      <th scope="col">Request type</th>
			      <th scope="col">link</th>		    
			    </tr>
			  </thead>
			  <tbody>
			  	<tr>		  
			      <td>
			      	<h5><span class="badge badge-primary">Get</span></h5>
			      </td>
			      <td>
			      	<code>api/comments</code>
			    	<p>Get all comments Note: this is for testing purpose only. It will be remove upon live</p>
			      </td>		    
			    </tr>
			    <tr>		  
			      <td>
			      	<h5><span class="badge badge-primary">Get</span></h5>
			      </td>
			      <td>
			      	<code>api/comments/{postId}/{postType}</code>
			    	<p>Get all comments on specific post</p>
			      </td>		    
			    </tr>
			    <tr>
			    <tr>		  
			      <td>
			      	<h5><span class="badge badge-primary">Get</span></h5>
			      </td>
			      <td>
			      	<code>api/comments/{postId}/{commentId}/{postType}</code>
			    	<p>Get all comment reply on specific comment</p>
			      </td>		    
			    </tr>		   
			    <tr>		  
			      <td>
			      	<h5><span class="badge badge-success">POST</span></h5>
			      </td>
			      <td>
			      	<code>api/comment/store</code>
			      	<p>Post a comment</p>
			      	<code>content:text|required</code><br>
			      	<code>commentable_id:{postID}</code><br>
			      	<code>commentable_type:App\{type}</code>
			    	<p>Fields</p>
			      </td>		    
			    </tr>
			    <tr>		  
			      <td>
			      	<h5><span class="badge badge-success">POST</span></h5>
			      </td>
			      <td>
			      	<code>api/comment/reply/store</code>
			      	<p>Post a reply on comment</p>
			      	<code>content:text|required</code><br>
			      	<code>parent_id:{parentID}</code><br>
			      	<code>commentable_id:{postID}</code><br>
			      	<code>commentable_type:App\{type}</code>
			    	<p>Fields</p>
			      </td>		    
			    </tr>
			    <tr>		  
			      <td>
			      	<h5><span class="badge badge-success">POST</span></h5>
			      </td>
			      <td>
			      	<code>api/comment/filestore</code>
			      	<p>Post an image or audio comment</p>
			      	<code>content:file|required</code><br>
			      	<code>commentable_id:{postID}</code><br>
			      	<code>commentable_type:App\{type}</code>
			    	<p>Fields</p>
			      </td>		    
			    </tr>
			    <tr>		  
			      <td>
			      	<h5><span class="badge badge-warning">PUT</span></h5>
			      </td>
			      <td>
			      	<code>api/comment/update/{commentId}</code>
			      	<p>Edit a comment</p>
			      </td>		    
			    </tr>
			    <tr>		  
			      <td>
			      	<h5><span class="badge badge-danger">DELETE</span></h5>
			      </td>
			      <td>
			      	<code>api/comment/delete/{commentId}</code>
			      	<p>Delete a comment</p>
			      </td>		    
			    </tr>
			    		    
			  </tbody>
			</table>
     	</div>
      </div>
    </div>
  </div>
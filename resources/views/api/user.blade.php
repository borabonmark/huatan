<div class="card">
    <div class="card-header" id="headingThree">
      <h5 class="mb-0">
        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFour">
          User Route
        </button>
      </h5>
    </div>
    <div id="collapseFive" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
      <div class="card-body">
        <div class="col align-self-center">
     		<table class="table table-hover">
			  <thead>
			    <tr>		      
			      <th scope="col">Request type</th>
			      <th scope="col">link</th>		    
			    </tr>
			  </thead>
			  <tbody>
			    <tr>		  
			      <td>
			      	<h5><span class="badge badge-primary">Get</span></h5>
			      </td>
			      <td>
			      	<code>api/user/{userId}</code>
			    	<p>Get public details on specific user</p>
			      </td>		    
			    </tr>
			    <tr>		  
			      <td>
			      	<h5><span class="badge badge-primary">Get</span></h5>
			      </td>
			      <td>
			      	<code>api/user</code>
			    	<p>Get private details on specific user | required login</p>
			      </td>		    
			    </tr>

			    <tr>		  
			      <td>
			      	<h5><span class="badge badge-success">POST</span></h5>
			      </td>
			      <td>
			      	<code>api/user/update</code>
			      	<p>Update user profile | required login</p>
			      	<code>avatar:file|required</code><br>
			      	<code>firstname:text|required</code><br>
			      	<code>lastname:text|required</code><br>
			      	<code>age:int|required</code><br>
			      	<code>height:float|required</code><br>
			      	<code>weight:float|required</code><br>
			      	<code>status:text|required</code>
			    	<p>Fields</p>
			      </td>		    
			    </tr>
			    <tr>		  
			      <td>
			      	<h5><span class="badge badge-success">POST</span></h5>
			      </td>
			      <td>
			      	<code>api/user/uploadimage</code>
			      	<p>Update user gallery | required login</p>
			      	<code>photo:file|required</code><br>
			    	<p>Fields</p>
			      </td>		    
			    </tr>
			    <tr>		  
			      <td>
			      	<h5><span class="badge badge-success">POST</span></h5>
			      </td>
			      <td>
			      	<code>api/user/changepassword</code>
			      	<p>Update user password | required login</p>
			      	<code>password:text|required</code><br>
			    	<p>Fields</p>
			      </td>		    
			    </tr>

			  </tbody>
			</table>
     	</div>
      </div>
    </div>
  </div>
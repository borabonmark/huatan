@extends('layouts.error')

@section('content')
<div class="vertical-align-wrap">
    <div class="vertical-align-middle">
    	<div class="center-box">
    		<div class="left">
                <div class="content">
                    <div class="header">
                    	<h1>{{ __('Error 403') }}</h1>
                    	<p>Sorry, but you are not authorized to view this page.<br />
                    	   <a href="{{ url('/') }}"><i class="fa fa-arrow-left"></i> Back to Homepage</a>
                    	</p>
                    </div>
                </div>
            </div>
            <div class="right errorpage">
                <div class="overlay"></div>
            </div>
    	</div>
    </div>
</div>
@endsection
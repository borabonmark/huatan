@extends('layouts.error')

@section('content')
<div class="vertical-align-wrap">
    <div class="vertical-align-middle">
    	<div class="center-box">
    		<div class="left">
                <div class="content">
                    <div class="header">
                    	<h1>{{ __('Error 404') }}</h1>
                    	<p>Page not found <br />
                    		<a href="{{ url('/') }}"><i class="fa fa-arrow-left"></i> Back to Homepage</a>
                    	</p>
                    </div>
                </div>
            </div>
            <div class="right errorpage">
                <div class="overlay"></div>
            </div>
    	</div>
    </div>
</div>
@endsection
@extends('layouts.app')

@section('content')
	<h3 class="page-title">
		<i class="fa fa-file"></i> {{ __('New Article') }}

		<button type="button" id="article-submit-btn" class="btn btn-success float-right" >{{ __('Save') }}</button>
	</h3>
	<div class="row">
		<div class="col-md-7">
			<div class="panel">
				<div class="panel-heading">
					<h3 class="panel-title">
						<strong><i class="fa fa-file"></i> {{ __('Article Information') }}</strong>
					</h3>
				</div>
				<div class="panel-body">
					@if (session('status'))
				        @component('alert', ['type' => session('status')])
				            {!! session('message') !!}
				        @endcomponent
				    @endif

					@include('articles.form', [
						'method' => 'POST',
						'action' => route('articles.store'),
						'input' => [
							'recommended' => old('recommended'),
							'category_id' => old('category_id'),
							'title' =>  old('title'),
							'summary' =>  old('summary'),
							'content' => old('content'),
							'publish' => old('publish'),
							'date_time_published' => old('date_time_published')
						]
					])
				</div>
			</div>
		</div>
		<div class="col-md-5">
			<div class="panel">
				<div class="panel-heading">
					<h3 class="panel-title">
						<strong><i class="fa fa-image"></i> {{ __('Photos') }}</strong>
					</h3>
				</div>
				<div class="panel-body">
					<form action="{{ route('images.upload') }}" class="dropzone" id="image-upload-form">
					    @csrf
					    <input type="hidden" name="width" value="{{ $photo['width'] }}" />
					    <input type="hidden" name="height" value="{{ $photo['height'] }}" />
					    <input type="hidden" name="size" value="{{ $photo['size'] }}" />
					</form>

					<div id="photos-thumbnail" class="margin-top-30"></div>
				</div>
			</div>

			<div class="panel">
				<div class="panel-heading">
					<h3 class="panel-title">
						<strong><i class="fa fa-tags"></i> {{ __('Tags') }}</strong>
					</h3>
				</div>
				<div class="panel-body">
					<form action="#" id="tag-form">
						<div class="input-group">
							<input type="text" class="form-control">
							<span class="input-group-btn">
								<button class="btn" type="button">{{ __('Add') }}</button>
							</span>
						</div>
					</form>

					<div id="tags-label"></div>
				</div>
			</div>
		</div>
	</div>
@endsection

@push('css')
<link href="{{ asset('vendor/jquery-ui/css/jquery-ui.css') }}" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('vendor/bootstrap/css/bootstrap-datetimepicker.min.css') }}">
<link href="{{ asset('vendor/dropzone/css/dropzone.css') }}" rel="stylesheet">
<link href="{{ asset('vendor/ekko-lightbox/css/ekko-lightbox.css') }}" rel="stylesheet">
@endpush

@push('script')
<script src="{{ asset('vendor/jquery-ui/js/jquery-ui.js') }}"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/ckeditor/4.5.11/ckeditor.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/ckeditor/4.5.11/adapters/jquery.js"></script>
<script src="{{ asset('vendor/moment/moment.js') }}"></script>
<script src="{{ asset('vendor/bootstrap/js/bootstrap-datetimepicker.js') }}"></script>
<script src="{{ asset('vendor/dropzone/js/dropzone.locale.js') }}"></script>
<script src="{{ asset('vendor/ekko-lightbox/js/ekko-lightbox.min.js') }}"></script>
<script>
	/**
	 * photo information
	 */
	 var photo = {
	 	width: "{{ $photo['width'] }}",
	 	height: "{{ $photo['height'] }}",
	 	size: "{{ $photo['size'] }}"
	 }

	/**
	 * Submit article form
	 */
	 $('#article-submit-btn').on('click', function(){
	 	$('#article-form').submit();
	 });

	/**
	 * Photos Upload
	 */
	Dropzone.autoDiscover = false;
	var remove = "{{ __('remove') }}";
	var defaultMessage = "{{ __('Drag and drop or click to upload.') }}";
	var defaultMessageDimension = "{{ __('Dimension atleast 360x300 pixels. Maximum file size of 2MB per image.') }}";
    var imageUploadFormDropzone = new Dropzone('#image-upload-form', {
        paramName: "file",
        maxFilesize: photo.size, //MB
        acceptedFiles: "image/*",
        addRemoveLinks: true,
        dictRemoveFile: "remove",
        dictDefaultMessage: "<i class='fa fa-camera'></i><p>" + defaultMessage + " <br/> " + defaultMessageDimension,
        success: function(result, url) {
            $('#photos-thumbnail').prepend(
                '<div class="col-md-2 photo">' + 
                	'<a href="' + url.image + '" data-toggle="lightbox" data-gallery="article-gallery">' + 
                    	'<img src="' + url.thumbnail + '" alt="photo" class="img-thumbnail">' + 
                    '</a>' +
                    '<span class="remove photo-remove text-center"><i class="fa fa-trash"></i> ' + remove + '</span>' + 
                '</div>'
            );

            $('#photos-input').prepend('<input name="thumbnail[]" type="hidden" value="' + url.thumbnail + '"/>');
            $('#photos-input').prepend('<input name="photo[]" type="hidden" value="' + url.image + '"/>');
        }
    });

    imageUploadFormDropzone.on("complete", function(file) {
    	if ("success" == file.status) {
        	imageUploadFormDropzone.removeFile(file);
        }
    });

    $(document).on('click', '.photo-remove', function(){
    	var _this = $(this);
    	var thumbnail = _this.parent();
    	var thumbnailImage = thumbnail.find('img').attr('src');
    	var thumbnailInput = $('input[value="' + thumbnailImage + '"]');
    	var photoImage = thumbnailImage.replace(photo.width + 'x' + photo.height, '');
    	var photoInput = $('input[value="' + photoImage + '"]');

    	thumbnail.remove(); 
    	thumbnailInput.remove();
    	photoInput.remove();
    });

    /**
     * Date time picker
     */
    $(document).ready(function(){
	    $("#date_time_published").datetimepicker({
			format: "llll"
		});
    });

    /**
     * CKeditor
     */
	$(document).ready(function(){
		var route_prefix = "{{ url(config('lfm.url_prefix', config('lfm.prefix'))) }}";
		$('textarea[name=content]').ckeditor({
	        height: 400,
	        filebrowserImageBrowseUrl: route_prefix + '?type=Images',
	        filebrowserImageUploadUrl: route_prefix + '/upload?type=Images&_token={{csrf_token()}}',
	        filebrowserBrowseUrl: route_prefix + '?type=Files',
	        filebrowserUploadUrl: route_prefix + '/upload?type=Files&_token={{csrf_token()}}'
	    });
	});

	/**
	 * Tags
	 */
	var tagsLabel = $('#tags-label');
	var tagsInput = $('#tags-input');
	var tagForm = $('#tag-form');
	var tagInput = tagForm.find('input[type=text]');
	var tagAddBtn = tagForm.find('.input-group-btn');

	// autocomplete
    tagInput.autocomplete({
      source: "{{ route('tags.search') }}"
    });

	tagAddBtn.on('click', function(){
		var tagText = tagInput.val().trim();
		var tagExists = tagsInput.find('input[value="' + tagText + '"]');

		// clear value
		tagInput.val('');
		
		// check if empty
		if (tagText.length <= 0) {
			return;
		}

		// check if tag already exists
		if (tagExists.length > 0) {
			return;
		}

		tagsInput.append('<input name="tag[]" type="hidden" value="' + tagText + '"/>');
		tagsLabel.append('<label class="label label-default tag-label">' + 
							'<span class="tag-text">' + tagText + '</span>' + 
							'&nbsp;&nbsp;<span class="tag-remove">&times;</span>' + 
						 '<label/>');
	});

	$(document).on('click', '.tag-label .tag-remove', function(){
		var _this = $(this);
		var tagLabel = _this.parent();
		var tagLabelText = tagLabel.find('.tag-text');
		var tagInput = tagsInput.find('input[value="' + tagLabelText.html() + '"]');

		tagLabel.remove();
		tagInput.remove();
	});

	/**
	 * Photo Viewer
	 */
    $(document).on('click', '[data-toggle="lightbox"]', function(event) {
        event.preventDefault();
        $(this).ekkoLightbox();
    });
</script>
@endpush

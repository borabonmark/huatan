@extends('layouts.blank')

@section('content')
	<div class="article"> 
		{!! $article->content !!}
	</div>
@endsection
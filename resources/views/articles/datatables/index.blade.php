@extends('layouts.app')

@section('content')
	<h3 class="page-title">
		<div class="row">
			<div class="col-md-12">
				<i class="fa fa-file"></i> {{ __('News') }}

				@can('hasAccess', [App\Article::class, 'add'])
					<a href="{{ route('articles.create') }}" class="btn btn-primary">
						<i class="fa fa-plus-square"></i>
					</a>
				@endcan
			</div>
		</div>
	</h3>
	<div class="row">
		<div class="col-md-12">
			<div class="panel">
				<div class="panel-heading">
					@if (session('status'))
				        @component('alert', ['type' => session('status')])
				            {!! session('message') !!}
				        @endcomponent
				    @endif
				</div>

				<div class="panel-body">
					<table id="articles-table" class="table table-bordered table-hover table-striped">
						<thead>
							<tr>
								<th class="text-center" width="20%">{{ __('Title') }}</th>
								<th class="text-center" width="8%">{{ __('Category') }}</th>
								<th class="text-center" width="12%">{{ __('Author') }}</th>
								<th class="text-center" width="8%">
									{{ __('Recommended') }}
									<button class="btn-update-status" data-statustoupdate="recommended">
										<i class="fa fa-check-square"></i>
									</button>
								</th>
								<th class="text-center" width="8%">
									{{ __('Publish') }}
									<button class="btn-update-status" data-statustoupdate="publish">
										<i class="fa fa-check-square"></i>
									</button>
								</th>
								<th class="text-center" width="10%">{{ __('Date & Time Published') }}</th>
								<th class="text-center" width="10%">{{ __('Actions') }}</th>
							</tr>
						</thead>
					</table>	
				</div>
			</div>
		</div>
	</div>
@endsection

@push('html')
<div class="modal fade" id="deleteArticleModal" tabindex="-1" role="dialog" aria-labelledby="deleteArticleModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="deleteArticleModalLabel">{{ __('Delete Article') }}</h4>
			</div>
			<div class="modal-body">
				<form method="POST" action="" id="deleteArticleForm">
                    @csrf
                    @method('DELETE')
                    <p>{{ __('Are you sure you want to delete this article?') }}</p>
                    <button type="submit" class="btn btn-danger">{{ __('Delete') }}</button> &nbsp;
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('Cancel') }}</button>
                </form>
			</div>
		</div>
	</div>
</div>
@endpush

@push('css')
<link href="{{ asset('vendor/jquery-datatables/css/jquery.dataTables.min.css') }}" rel="stylesheet">
@endpush

@push('script')
<script src="{{ asset('vendor/jquery-datatables/js/jquery.dataTables.locale.js') }}"></script>
<script>
$(function() {
	/**
	 * List
	 */
	var articlesTable = $('#articles-table').DataTable({
		pageLength: 25,
	    processing: true,
	    serverSide: true,
	    ajax: '{!! route('articles.getList') !!}',
	    columnDefs: [
	    	{
	    		targets: [3, 4, 5, 6],
	    		className: 'dt-body-center dt-head-center'
	    	}
	    ],
	    columns: [
	    	{ data: 'title', name: 'title' },
	        { data: 'category', name: 'category' },
	        { data: 'author', name: 'author' },
	        { data: 'recommended', name: 'recommended', searchable: false, orderable: false },
	        { data: 'publish', name: 'publish', searchable: false, orderable: false },
	        { data: 'date_time_published', name: 'date_time_published', searchable: false, orderable: false },
	        { data: 'action', name: 'action', searchable: false, orderable: false }
	    ],

	});

	/**
	 * Delete Article
	 */
	$("#deleteArticleModal").on("show.bs.modal", function (event) {
	    var button = $(event.relatedTarget);
	    var url = "{{ url('/') }}/" + Lang.getLocale() + "/articles/" +  button.data("article");
	    $("#deleteArticleForm").attr("action", url);
	});

	/**
	 * Status Update
	 */
	 $('.btn-update-status').on('click', function(){
	 	var _this = $(this);
	 	var statusToUpdate = _this.data('statustoupdate');
	 	var articles = [];
	 	var checked = [];

	 	_this.html('<span class="loading"></span>');


	 	$('input[name="' + statusToUpdate + '[]"]').each(function(){
	 		var _this = $(this);

	 		articles.push(_this.val());

	 		if (_this.is(':checked')) {
	 			checked.push(_this.val());
	 		}
	 	});

	 	$.post(
	 		"{!! route('articles.statusUpdate') !!}", 
	 		{
	 			_token: "{{ csrf_token() }}",
	 			statusToUpdate: statusToUpdate,
	 			articles: articles,
	 			checked: checked
	 		}, 
	 		function(result, status){
				$('.panel-heading').html(
					'<div class="alert alert-' + result.status + ' alert-dismissible" role="alert">' +
						'<button type="button" class="close" data-dismiss="alert" aria-label="Close">' + 
							'<span aria-hidden="true">×</span>' + 
						'</button>' +
						'<i class="fa fa-check-circle"></i> ' + result.message + 
					'</div>'
			    );

			    _this.html('<i class="fa fa-check-square"></i>');

			    setTimeout(function(){ $('.panel-heading').html(''); }, 3000);
	 		}
	 	);
	});
});
</script>
@endpush

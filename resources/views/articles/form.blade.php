<form method="POST" action="{{ $action }}" id="article-form">
    @csrf
    <input type="hidden" name="_method" value="{{ $method }}">

    <div class="form-group  {{ $errors->has('recommended') ? 'has-error has-feedback' : '' }}">
        <div class="checkbox">
            <label for="recommended" class="control-label">
                <input type="checkbox" name="recommended" id="recommended" value="yes" 
                @if('yes' == $input['recommended']) checked @endif>
                <strong>{{ __('Set as recommended') }}</strong>
            </label>
        </div>

        @if ($errors->has('recommended'))
            <span class="help-block text-left">{{ $errors->first('recommended') }}</span>
        @endif
    </div>

    <div class="form-group  {{ $errors->has('title') ? 'has-error has-feedback' : '' }}">
        <label for="name" class="control-label">{{ __('Title') }}</label>
        <input type="text" name="title" id="title" class="form-control" value="{{ $input['title'] }}" required>

        @if ($errors->has('title'))
            <span class="help-block text-left">{{ $errors->first('title') }}</span>
        @endif
    </div>

    <div class="form-group  {{ $errors->has('category_id') ? 'has-error has-feedback' : '' }}">
        <label for="category_id" class="control-label">{{ __('Category') }}</label>
        <select name="category_id" id="category_id" class="form-control" required>
            <option value="">{{ __('Select') }}</option>
            @if(!empty($categories))
                @foreach($categories as $category)
                    <option value="{{ $category->id }}"  
                    @if ($input['category_id'] == $category->id) {{ 'selected' }} @endif>
                        {{ $category->name }}
                    </option>
                @endforeach
            @endif
        </select>

        @if ($errors->has('category_id'))
            <span class="help-block text-left">{{ $errors->first('category_id') }}</span>
        @endif
    </div>

    <div class="form-group  {{ $errors->has('publish') ? 'has-error has-feedback' : '' }}">
        <label for="publish" class="control-label">{{ __('Publish') }}</label>
        <label class="fancy-radio">
            <input type="radio" name="publish" value="yes" @if('yes' == $input['publish']) checked @endif>
            <span><i></i>{{ __('Yes') }}</span>
        </label>
        <label class="fancy-radio">
            <input type="radio" name="publish" value="no" @if('no' == $input['publish']) checked @endif>
            <span><i></i>{{ __('No') }}</span>
        </label>

        @if ($errors->has('publish'))
            <span class="help-block text-left">{{ $errors->first('publish') }}</span>
        @endif
    </div>

    <div class="form-group  {{ $errors->has('date_time_published') ? 'has-error has-feedback' : '' }}">
        <label for="date_time_published" class="control-label">{{ __('Date & Time Published') }}</label>
        <input type="text" name="date_time_published" id="date_time_published" class="form-control" value="{{ $input['date_time_published'] }}" required>

        @if ($errors->has('date_time_published'))
            <span class="help-block text-left">{{ $errors->first('date_time_published') }}</span>
        @endif
    </div>

    <div class="form-group  {{ $errors->has('summary') ? 'has-error has-feedback' : '' }}">
        <label for="summary" class="control-label">{{ __('Summary') }}</label>
        <textarea name="summary" id="summary" class="form-control" required>{{ $input['summary'] }}</textarea>

        @if ($errors->has('summary'))
            <span class="help-block text-left">{{ $errors->first('summary') }}</span>
        @endif
    </div>

    <div class="form-group  {{ $errors->has('content') ? 'has-error has-feedback' : '' }}">
        <label for="content" class="control-label">{{ __('Content') }}</label>
        <textarea name="content" id="content" class="form-control" required>{{ $input['content'] }}</textarea>

        @if ($errors->has('content'))
            <span class="help-block text-left">{{ $errors->first('content') }}</span>
        @endif
    </div>

    <div id="photos-input">
        @if (!empty($input['photos']))
            @foreach($input['photos'] as $photo)
                <input name="photo[]" type="hidden" value="{{ $photo->value }}"/>
            @endforeach
        @endif

        @if (!empty($input['thumbnails']))
            @foreach($input['thumbnails'] as $thumbnail)
                <input name="thumbnail[]" type="hidden" value="{{ $thumbnail->value }}"/>
            @endforeach
        @endif
    </div>

    <div id="tags-input">
        @if (!empty($input['tags']))
            @foreach($input['tags'] as $tag)
                <input name="tag[]" type="hidden" value="{{ $tag->name }}"/>
            @endforeach
        @endif
    </div>
</form>

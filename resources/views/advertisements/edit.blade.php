@extends('layouts.app')

@section('content')
	<h3 class="page-title">
		<i class="fa fa-tv"></i> {{ __('Edit Advertisement') }}

		@can('hasAccess', [App\Advertisement::class, 'add'])
			<a href="{{ route('advertisements.create') }}" class="btn btn-primary">
				<i class="fa fa-plus-square"></i>
			</a>
		@endcan
	</h3>
	<div class="row">
		<div class="col-md-7">
			<div class="panel">
				<div class="panel-heading">
					<h3 class="panel-title">
						<strong><i class="fa fa-file"></i> {{ __('Advertisement Information') }}</strong>
					</h3>
				</div>
				<div class="panel-body">
					@if (session('status'))
				        @component('alert', ['type' => session('status')])
				            {!! session('message') !!}
				        @endcomponent
				    @endif

					@include('advertisements.form', [
						'method' => 'PATCH',
						'action' => route('advertisements.update', ['advertisement' => $advertisement->id]),
						'button' => 'Update',
						'input' => [
							'name' =>  $advertisement->name,
							'title' => $advertisement->title,
							'url' => $advertisement->url,
							'date_time_online' => $advertisement->date_time_online->format('D, M j, Y g:i A'),
							'status' => $advertisement->status,
						]
					])
				</div>
			</div>
		</div>
		<div class="col-md-5">
			<div class="panel">
				<div class="panel-heading">
					<h3 class="panel-title">
						<strong><i class="fa fa-image"></i> {{ __('Material') }}</strong>
					</h3>
				</div>
				<div class="panel-body">
					<a href="{{ $advertisement->material }}" data-toggle="lightbox">
						<img src="{{ $advertisement->material }}" id="material" class="img-responsive" alt="{{ $advertisement->name }} material">
					</a>
				</div>
			</div>
		</div>
	</div>
@endsection

@push('css')
<link href="{{ asset('vendor/bootstrap/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
<link href="{{ asset('vendor/ekko-lightbox/css/ekko-lightbox.css') }}" rel="stylesheet">
@endpush

@push('script')
<script src="{{ asset('vendor/moment/moment.js') }}"></script>
<script src="{{ asset('vendor/bootstrap/js/bootstrap-datetimepicker.js') }}"></script>
<script src="{{ asset('vendor/ekko-lightbox/js/ekko-lightbox.min.js') }}"></script>
<script>
	$(document).ready(function(){
		$("#date_time_online").datetimepicker({
			format: "llll"
		});
	});

	$(document).on('click', '[data-toggle="lightbox"]', function(event) {
        event.preventDefault();
        $(this).ekkoLightbox();
    });
</script>
@endpush

@extends('layouts.app')

@section('content')
	<h3 class="page-title">
		<div class="row">
			<div class="col-md-12">
				<i class="fa fa-tv"></i> {{ __('Advertisements') }}

				@can('hasAccess', [App\Advertisement::class, 'add'])
					<a href="{{ route('advertisements.create') }}" class="btn btn-primary">
						<i class="fa fa-plus-square"></i>
					</a>
				@endcan
			</div>
		</div>
	</h3>
	<div class="row">
		<div class="col">
			<div class="panel">
				<div class="panel-heading">
					@if (session('status'))
				        @component('alert', ['type' => session('status')])
				            {!! session('message') !!}
				        @endcomponent
				    @endif
				</div>
				<div class="panel-body">
					<table id="advertisements-table" class="table table-bordered table-hover table-striped">
						<thead>
							<tr>
								<th class="text-center" width="20%">{{ __('Name') }}</th>
								<th class="text-center" width="20%">{{ __('Title') }}</th>
								<th class="text-center" width="15%">{{ __('Date & Time Online') }}</th>
								<th class="text-center" width="8%">
									{{ __('Status') }}
									<button class="btn-update-status" data-statustoupdate="status">
										<i class="fa fa-check-square"></i>
									</button>
								</th>
								<th class="text-center" width="12%">{{ __('Date Created') }}</th>
								<th class="text-center" width="12%">{{ __('Actions') }}</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
	</div>
@endsection

@push('html')
<div class="modal fade" id="deleteAdvertisementsModal" tabindex="-1" role="dialog" aria-labelledby="deleteAdvertisementsModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="deleteAdvertisementsModalLabel">{{ __('Delete Advertisement') }}</h4>
			</div>
			<div class="modal-body">
				<form method="POST" action="" id="deleteAdvertisementForm">
                    @csrf
                    @method('DELETE')
                    <p>{{ __('Are you sure you want to delete this advertisement?') }}</p>
                    <button type="submit" class="btn btn-danger">{{ __('Delete') }}</button> &nbsp;
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('Cancel') }}</button>
                </form>
			</div>
		</div>
	</div>
</div>
@endpush

@push('css')
<link href="{{ asset('vendor/jquery-datatables/css/jquery.dataTables.min.css') }}" rel="stylesheet">
@endpush

@push('script')
<script src="{{ asset('vendor/jquery-datatables/js/jquery.dataTables.locale.js') }}"></script>
<script>
$(function() {
	/**
	 * List
	 */
    var advertisementsTable = $('#advertisements-table').DataTable({
    	pageLength: 25,
        processing: true,
        serverSide: true,
        ajax: '{!! route('advertisements.getList') !!}',
        columnDefs: [
        	{
        		targets: [2, 3, 4, 5],
        		className: 'dt-body-center dt-head-center'
        	}
        ],
        columns: [
        	{ data: 'name', name: 'name' },
            { data: 'title', name: 'title' },
            { data: 'date_time_online', name: 'date_time_online' },
            { data: 'status', name: 'status', searchable: false, orderable: false },
            { data: 'created_at', name: 'created_at', searchable: false, orderable: false },
            { data: 'action', name: 'action', searchable: false, orderable: false }
        ],

    });

	/**
	 * Delete Article
	 */
	$("#deleteAdvertisementsModal").on("show.bs.modal", function (event) {
	    var button = $(event.relatedTarget);
	    var url = "{{ url('/') }}/" + Lang.getLocale() + "/advertisements/" +  button.data("advertisement");
	    $("#deleteAdvertisementForm").attr("action", url);
	});

	/**
	 * Status Update
	 */
	 $('.btn-update-status').on('click', function(){
	 	var _this = $(this);
	 	var statusToUpdate = _this.data('statustoupdate');
	 	var advertisements = [];
	 	var checked = [];

	 	_this.html('<span class="loading"></span>');


	 	$('input[name="' + statusToUpdate + '[]"]').each(function(){
	 		var _this = $(this);

	 		advertisements.push(_this.val());

	 		if (_this.is(':checked')) {
	 			checked.push(_this.val());
	 		}
	 	});

	 	$.post(
	 		"{!! route('advertisements.statusUpdate') !!}", 
	 		{
	 			_token: "{{ csrf_token() }}",
	 			statusToUpdate: statusToUpdate,
	 			advertisements: advertisements,
	 			checked: checked
	 		}, 
	 		function(result, status){
				$('.panel-heading').html(
					'<div class="alert alert-' + result.status + ' alert-dismissible" role="alert">' +
						'<button type="button" class="close" data-dismiss="alert" aria-label="Close">' + 
							'<span aria-hidden="true">×</span>' + 
						'</button>' +
						'<i class="fa fa-check-circle"></i> ' + result.message + 
					'</div>'
			    );

			    _this.html('<i class="fa fa-check-square"></i>');

			    setTimeout(function(){ $('.panel-heading').html(''); }, 3000);
	 		}
	 	);
	 });
 });
</script>
@endpush
<form method="POST" action="{{ $action }}" enctype="multipart/form-data">
    @csrf
    <input type="hidden" name="_method" value="{{ $method }}">

    <div class="form-group  {{ $errors->has('name') ? 'has-error has-feedback' : '' }}">
        <label for="name" class="control-label">{{ __('Name') }}</label>
        <input type="text" name="name" id="name" class="form-control" value="{{ $input['name'] }}" required>

        @if ($errors->has('name'))
            <span class="help-block text-left">{{ $errors->first('name') }}</span>
        @endif
    </div>

    <div class="form-group  {{ $errors->has('title') ? 'has-error has-feedback' : '' }}">
        <label for="title" class="control-label">{{ __('Title') }}</label>
        <input type="text" name="title" id="title" class="form-control" value="{{ $input['title'] }}" required>

        @if ($errors->has('title'))
            <span class="help-block text-left">{{ $errors->first('title') }}</span>
        @endif
    </div>

    <div class="form-group  {{ $errors->has('url') ? 'has-error has-feedback' : '' }}">
        <label for="url" class="control-label">{{ __('URL') }}</label>
        <input type="text" name="url" id="url" class="form-control" value="{{ $input['url'] }}" required>
        <p class="help-block">{{ __('Must start with http:// or https://') }}</p>

        @if ($errors->has('url'))
            <span class="help-block text-left">{{ $errors->first('url') }}</span>
        @endif
    </div>

    <div class="form-group  {{ $errors->has('date_time_online') ? 'has-error has-feedback' : '' }}">
        <label for="date_time_online" class="control-label">{{ __('Date & Time Online') }}</label>
        <input type="text" name="date_time_online" id="date_time_online" class="form-control" value="{{ $input['date_time_online'] }}" required>

        @if ($errors->has('date_time_online'))
            <span class="help-block text-left">{{ $errors->first('date_time_online') }}</span>
        @endif
    </div>

    <div class="form-group  {{ $errors->has('status') ? 'has-error has-feedback' : '' }}">
        <label for="status" class="control-label">{{ __('Status') }}</label>
        <label class="fancy-radio">
            <input type="radio" name="status" value="online" @if('online' == $input['status']) checked @endif>
            <span><i></i>{{ __('Online') }}</span>
        </label>
        <label class="fancy-radio">
            <input type="radio" name="status" value="offline" @if('offline' == $input['status']) checked @endif>
            <span><i></i>{{ __('Offline') }}</span>
        </label>

        @if ($errors->has('status'))
            <span class="help-block text-left">{{ $errors->first('status') }}</span>
        @endif
    </div>

    <div class="form-group  {{ $errors->has('material') ? 'has-error has-feedback' : '' }}">
        <label for="material" class="control-label">{{ __('Material') }}</label>
        <input type="file" name="material" id="material" class="form-control">
        <p class="help-block">{{ __('Must be an image (jpeg, png, bmp, gif). Dimension atleast') }} {{ $material['width' ]}}x{{ $material['height'] }} {{ __('pixels and maximum file size') }} {{ $material['size'] }}MB</p>

        @if ($errors->has('material'))
            <span class="help-block text-left">{{ $errors->first('material') }}</span>
        @endif
    </div>

    <div class="form-group">
        <button type="submit" class="btn btn-success">{{ __($button) }}</button>
        @can('hasAccess', [App\Advertisement::class, 'view'])
            <a href="{{ route('advertisements.index') }}" class="btn btn-default">{{ __('Cancel') }}</a>
        @endcan
    </div>
</form>

@extends('layouts.app')

@section('content')
	<h3 class="page-title">
		<i class="fa fa-tv"></i> {{ __('New Advertisement') }}
	</h3>
	<div class="row">
		<div class="col-md-7">
			<div class="panel">
				<div class="panel-heading">
					<h3 class="panel-title">
						<strong><i class="fa fa-file"></i> {{ __('Advertisement Information') }}</strong>
					</h3>
				</div>
				<div class="panel-body">
					@if (session('status'))
				        @component('alert', ['type' => session('status')])
				            {!! session('message') !!}
				        @endcomponent
				    @endif

					@include('advertisements.form', [
						'method' => 'POST',
						'action' => route('advertisements.store'),
						'button' => 'Create',
						'input' => [
							'name' =>  old('name'),
							'title' => old('title'),
							'url' => old('url'),
							'date_time_online' => old('date_time_online'),
							'status' => old('status') ? old('status') : 'online'
						]
					])
				</div>
			</div>
		</div>
	</div>
@endsection

@push('css')
<link rel="stylesheet" href="{{ asset('vendor/bootstrap/css/bootstrap-datetimepicker.min.css') }}">
@endpush

@push('script')
<script src="{{ asset('vendor/moment/moment.js') }}"></script>
<script src="{{ asset('vendor/bootstrap/js/bootstrap-datetimepicker.js') }}"></script>
<script>
	$(document).ready(function(){
		$("#date_time_online").datetimepicker({
			format: "llll"
		});
	});
</script>
@endpush

<!doctype html>
<html lang="{{ app()->getLocale() }}" class="fullscreen-bg">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{ config('app.name', 'HT Manage') }}</title>

        <!-- Vendor CSS -->
        <link rel="stylesheet" href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ asset('vendor/font-awesome/css/font-awesome.min.css') }}">
        <link rel="stylesheet" href="{{ asset('vendor/linearicons/style.css') }}">

        <!-- Page level CSS -->
        @stack('css')

        <!-- CSS -->
        <link rel="stylesheet" href="{{ asset('css/admin.css') }}">

        <!-- Fonts CSS -->
        <link rel="stylesheet" href="{{ asset('css/fonts.css') }}">

        <!-- ICONS -->
        <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('apple-icon.png') }}">
        <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('favicon.png') }}">
    </head>
    <body>
        <div id="wrapper">
            <div id="locale-wrap">
                <div id="locale-middle">
                    <div id="locale-box">
                        <div class="btn-group">
                            <div class="btn-group">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                    {{ LaravelLocalization::getCurrentLocaleName() }} &nbsp;<span class="caret"></span>
                                </button>

                                <ul class="dropdown-menu">
                                    @include('partials.locales')
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            @yield('content')
        </div>

        <!-- Vendor JS -->
        <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
        <script src="{{ asset('vendor/bootstrap/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('vendor/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
        <script src="{{ asset('vendor/klorofil-common/klorofil-common.js') }}"></script>
    </body>
</html>
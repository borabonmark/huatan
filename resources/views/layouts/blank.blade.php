<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{ config('app.name', 'HT Manage') }}</title>

        <!-- Vendor CSS -->
        <link rel="stylesheet" href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}">

        <!-- CSS -->
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">

        <!-- ICONS -->
        <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('apple-icon.png') }}">
        <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('favicon.png') }}">
    </head>
    <body>
        <div class="container">
            @yield('content')
        </div>
        
        <!-- Vendor JS -->
        <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
        <script src="{{ asset('vendor/bootstrap/js/bootstrap.min.js') }}"></script>
    </body>
</html>
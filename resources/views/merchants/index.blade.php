@extends('layouts.app')

@section('content')
	<h3 class="page-title">
		<div class="row">
			<div class="col-md-8">
				<i class="fa fa-building"></i> {{ __('Merchants') }}

				@can('hasAccess', [App\Merchant::class, 'add'])
					<a href="{{ route('merchants.create') }}" class="btn btn-primary">
						<i class="fa fa-plus-square"></i>
					</a>
				@endcan
			</div>

			<div class="col-md-4">
				@include('merchants.search')
			</div>
		</div>
	</h3>
	<div class="row">
		<div class="col-md-12">
			<div class="panel">
				<div class="panel-heading">
					@if (session('status'))
				        @component('alert', ['type' => session('status')])
				            {!! session('message') !!}
				        @endcomponent
				    @endif
				</div>

				<div class="panel-body">
					<table class="table table-bordered table-hover table-striped">
						<thead>
							<tr>
								<th class="text-center" width="20%">{{ __('Merchant Name') }}</th>
								<th class="text-center" width="10%">{{ __('Category') }}</th>
								<th class="text-center" width="12%">{{ __('Phone') }}</th>
								<th class="text-center" width="20%">{{ __('Email') }}</th>
								<th class="text-center" width="8%">
									{{ __('Priority') }}
									<button class="btn-update-status" data-statustoupdate="priority">
										<i class="fa fa-check-square"></i>
									</button>
								</th>
								<th class="text-center" width="12%">{{ __('Date Created') }}</th>
								<th class="text-center">{{ __('Actions') }}</th>
							</tr>
						</thead>
						<tbody>
							@empty($merchants->total())
								<tr>
									<td colspan="8" class="text-center">{{ __('No merchants found.') }}</td>
								</tr>
							@else
								@foreach($merchants as $merchant)
									<tr class="text-center">
										<td>{{ $merchant->name }}</td>
										<td>{{ $merchant->category->name }}</td>
										<td><a href="tel:{{$merchant->phone}}">{{ $merchant->phone }}</a></td>
										<td><a href="mailto:{{ $merchant->email }}">{{ $merchant->email }}</a></td>
										<td>
											<input type="checkbox" name="priority[]" value="{{ $merchant->id }}" @if ('yes' == $merchant->getMetaByKey('priority')) checked @endif>
										</td>
										<td>{{ $merchant->created_at }}</td>
										<td class="text-center">
											@can('hasAccess', [App\Merchant::class, 'edit'])
												<a href="{{ route('merchants.edit', ['merchant' => $merchant->id]) }}">
													<i class="fa fa-pencil"></i> {{ __('Edit') }}
												</a> &nbsp;
											@endcan

											@can('hasAccess', [App\Merchant::class, 'delete'])
												<a href="javascript:void(0);" data-toggle="modal" data-target="#deleteMerchantModal" data-merchant="{{ $merchant->id }}">
		                                            <i class="fa fa-trash"></i> {{ __('Delete') }}
		                                        </a> &nbsp;
	                                        @endcan
										</td>
									</tr>
								@endforeach
							@endempty
						</tbody>
					</table>
					{{ $merchants->links() }}
				</div>
			</div>
		</div>
	</div>
@endsection

@push('html')
<div class="modal fade" id="deleteMerchantModal" tabindex="-1" role="dialog" aria-labelledby="deleteMerchantModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="deleteMerchantModalLabel">{{ __('Delete Merchant') }}</h4>
			</div>
			<div class="modal-body">
				<form method="POST" action="" id="deleteMerchantForm">
                    @csrf
                    @method('DELETE')
                    <p>{{ __('Are you sure you want to delete this merchant?') }}</p>
                    <button type="submit" class="btn btn-danger">{{ __('Delete') }}</button> &nbsp;
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('Cancel') }}</button>
                </form>
			</div>
		</div>
	</div>
</div>
@endpush

@push('script')
<script>
/**
 * Delete Article
 */
$("#deleteMerchantModal").on("show.bs.modal", function (event) {
    var button = $(event.relatedTarget);
    $("#deleteMerchantForm").attr("action", "{{ url('/merchants/') }}/" + button.data("merchant"));
});

/**
 * Status Update
 */
 $('.btn-update-status').on('click', function(){
 	var _this = $(this);
 	var statusToUpdate = _this.data('statustoupdate');
 	var merchants = [];
 	var checked = [];

 	_this.html('<span class="loading"></span>');


 	$('input[name="' + statusToUpdate + '[]"]').each(function(){
 		var _this = $(this);

 		merchants.push(_this.val());

 		if (_this.is(':checked')) {
 			checked.push(_this.val());
 		}
 	});

 	$.post(
 		"{!! route('merchants.statusUpdate') !!}", 
 		{
 			_token: "{{ csrf_token() }}",
 			statusToUpdate: statusToUpdate,
 			merchants: merchants,
 			checked: checked
 		}, 
 		function(result, status){
			$('.panel-heading').html(
				'<div class="alert alert-' + result.status + ' alert-dismissible" role="alert">' +
					'<button type="button" class="close" data-dismiss="alert" aria-label="Close">' + 
						'<span aria-hidden="true">×</span>' + 
					'</button>' +
					'<i class="fa fa-check-circle"></i> ' + result.message + 
				'</div>'
		    );

		    _this.html('<i class="fa fa-check-square"></i>');

		    setTimeout(function(){ $('.panel-heading').html(''); }, 3000);
 		}
 	);
 });
</script>
@endpush
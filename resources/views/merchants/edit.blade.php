@extends('layouts.app')

@section('content')
	<h3 class="page-title">
		<i class="fa fa-building"></i> {{ __('Edit Merchant') }}

		@can('hasAccess', [App\Merchant::class, 'add'])
			<a href="{{ route('merchants.create') }}" class="btn btn-primary">
				<i class="fa fa-plus-square"></i>
			</a>
		@endcan

		<button type="button" id="merchant-submit-btn" class="btn btn-success float-right" >{{ __('Save Changes') }}</button>
	</h3>
	<div class="row">
		<div class="col-md-7">
			<div class="panel">
				<div class="panel-heading">
					<h3 class="panel-title">
						<strong><i class="fa fa-file"></i> {{ __('Merchant Information') }}</strong>
					</h3>
				</div>
				<div class="panel-body">
					@if (session('status'))
				        @component('alert', ['type' => session('status')])
				            {!! session('message') !!}
				        @endcomponent
				    @endif

					@include('merchants.form', [
						'method' => 'PATCH',
						'action' => route('merchants.update', ['merchant' => $merchant->id]),
						'input' => [
							'priority' => $merchant->getMetaByKey('priority'),
							'name' =>  $merchant->name,
							'category_id' => $merchant->category_id,
							'phone' => $merchant->phone,
							'email' => $merchant->email,
							'address' => $merchant->address,
							'description' => $merchant->description,
							'activity' => $merchant->activity,
							'photos' => $merchant->photos(),
							'thumbnails' => $merchant->thumbnails(),
							'tags' => $merchant->tags,
							'longitude' => $merchant->getMetaByKey('longitude'),
							'latitude' => $merchant->getMetaByKey('latitude')
						]
					])
				</div>
			</div>
		</div>
		<div class="col-md-5">
			<div class="panel">
				<div class="panel-heading">
					<h3 class="panel-title">
						<strong><i class="fa fa-image"></i> {{ __('Logo') }}</strong>
					</h3>
				</div>
				<div class="panel-body">
					<a href="{{ $merchant->logo }}" data-toggle="lightbox">
						<img src="{{ $merchant->logo }}" id="logo" class="img-responsive" alt="{{ $merchant->name }} logo">
					</a>
				</div>
			</div>

			<div class="panel">
				<div class="panel-heading">
					<h3 class="panel-title">
						<strong><i class="fa fa-image"></i> {{ __('Photos') }}</strong>
					</h3>
				</div>
				<div class="panel-body">
					<form action="{{ route('images.upload') }}" class="dropzone" id="image-upload-form">
					    @csrf
					    <input type="hidden" name="width" value="{{ $photo['width'] }}" />
					    <input type="hidden" name="height" value="{{ $photo['height'] }}" />
					    <input type="hidden" name="size" value="{{ $photo['size'] }}" />
					</form>

					<div id="photos-thumbnail" class="margin-top-30">
						@if (!empty($thumbnails = $merchant->thumbnails()) && 
						     !empty($photos = $merchant->photos()))
				            @foreach($thumbnails as $key => $thumbnail)
				                <div class="col-md-2 photo">
				                	<a href="{{ $photos[$key]->value }}" data-toggle="lightbox" data-gallery="merchant-gallery">
				                    	<img src="{{ $thumbnail->value }}" alt="photo" class="img-thumbnail">
				                	</a>
				                    <span class="remove photo-remove text-center"><i class="fa fa-trash"></i> {{ __('remove') }}</span>
				                </div>
				            @endforeach
				        @endif
					</div>
				</div>
			</div>

			<div class="panel">
				<div class="panel-heading">
					<h3 class="panel-title">
						<strong><i class="fa fa-tags"></i> {{ __('Tags') }}</strong>
					</h3>
				</div>
				<div class="panel-body">
					<form action="#" id="tag-form">
						<div class="input-group">
							<input type="text" class="form-control">
							<span class="input-group-btn">
								<button class="btn" type="button">{{ __('Add') }}</button>
							</span>
						</div>
					</form>

					<div id="tags-label">
						@if (!empty($tags = $merchant->tags))
				            @foreach($tags as $tag)
				            	<label class="label label-default tag-label">
				            		<span class="tag-text">{{ $tag->name }}</span>
				            		&nbsp;&nbsp;<span class="tag-remove">&times;</span>
				            	</label>
				            @endforeach
				        @endif
					</div>
				</div>
			</div>

			<div class="panel">
				<div class="panel-heading">
					<h3 class="panel-title">
						<strong><i class="fa fa-comments"></i> {{ __('Comments') }}</strong>
					</h3>
				</div>
				<div class="panel-body">
					<div id="comments">
						<div class="window">
							<ul class="list"></ul>
						</div>
						<span class="loading"></span>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@push('html')
<div class="modal fade" id="deleteCommentModal" tabindex="-1" role="dialog" aria-labelledby="deleteCommentModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="deleteCommentModalLabel">{{ __('Delete Comment') }}</h4>
			</div>
			<div class="modal-body">
                <p>{{ __('Are you sure you want to delete this comment?') }}</p>
                <button type="submit" id="btnDeleteComment" class="btn btn-danger">{{ __('Delete') }}</button> &nbsp;
                <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('Cancel') }}</button>
			</div>
		</div>
	</div>
</div>
@endpush

@push('css')
<link href="{{ asset('vendor/jquery-ui/css/jquery-ui.css') }}" rel="stylesheet">
<link href="{{ asset('vendor/dropzone/css/dropzone.css') }}" rel="stylesheet">
<link href="{{ asset('vendor/ekko-lightbox/css/ekko-lightbox.css') }}" rel="stylesheet">
@endpush

@push('script')
<script src="{{ asset('vendor/jquery-ui/js/jquery-ui.js') }}"></script>
<script src="{{ asset('vendor/dropzone/js/dropzone.locale.js') }}"></script>
<script src="{{ asset('vendor/moment/moment.js') }}"></script>
<script src="{{ asset('vendor/ekko-lightbox/js/ekko-lightbox.min.js') }}"></script>
<script>
	/**
	 * photo information
	 */
	 var photo = {
	 	width: "{{ $photo['width'] }}",
	 	height: "{{ $photo['height'] }}",
	 	size: "{{ $photo['size'] }}"
	 }

	/**
	 * Submit article form
	 */
	 $('#merchant-submit-btn').on('click', function(){
	 	$('#merchant-form').submit();
	 });

	/**
	 * Photos Upload
	 */
	Dropzone.autoDiscover = false;
	var remove = "{{ __('remove') }}";
	var defaultMessage = "{{ __('Drag and drop or click to upload.') }}";
	var defaultMessageDimension = "{{ __('Dimension atleast 360x300 pixels. Maximum file size of 2MB per image.') }}";
    var imageUploadFormDropzone = new Dropzone('#image-upload-form', {
        paramName: "file",
        maxFilesize: photo.size, //MB
        acceptedFiles: "image/*",
        addRemoveLinks: true,
        dictRemoveFile: "remove",
        dictDefaultMessage: "<i class='fa fa-camera'></i><p>" + defaultMessage + " <br/> " + defaultMessageDimension,
        success: function(result, url) {
            $('#photos-thumbnail').prepend(
                '<div class="col-md-2 photo">' + 
                	'<a href="' + url.image + '" data-toggle="lightbox" data-gallery="merchant-gallery">' + 
                    	'<img src="' + url.thumbnail + '" alt="photo" class="img-thumbnail">' + 
                    '</a>' +
                    '<span class="remove photo-remove text-center"><i class="fa fa-trash"></i> ' + remove + '</span>' + 
                '</div>'
            );

            $('#photos-input').prepend('<input name="thumbnail[]" type="hidden" value="' + url.thumbnail + '"/>');
            $('#photos-input').prepend('<input name="photo[]" type="hidden" value="' + url.image + '"/>');
        }
    });

    imageUploadFormDropzone.on("complete", function(file) {
    	if ("success" == file.status) {
    		imageUploadFormDropzone.removeFile(file);
    	}
    });

    $(document).on('click', '.photo-remove', function(){
    	var _this = $(this);
    	var thumbnail = _this.parent();
    	var thumbnailImage = thumbnail.find('img').attr('src');
    	var thumbnailInput = $('input[value="' + thumbnailImage + '"]');
    	var photoImage = thumbnailImage.replace(photo.width + 'x' + photo.height, '');
    	var photoInput = $('input[value="' + photoImage + '"]');

    	thumbnail.remove(); 
    	thumbnailInput.remove();
    	photoInput.remove();
    });

    /**
	 * Tags
	 */
	var tagsLabel = $('#tags-label');
	var tagsInput = $('#tags-input');
	var tagForm = $('#tag-form');
	var tagInput = tagForm.find('input[type=text]');
	var tagAddBtn = tagForm.find('.input-group-btn');

	// autocomplete
    tagInput.autocomplete({
      source: "{{ route('tags.search') }}"
    });

	tagAddBtn.on('click', function(){
		var tagText = tagInput.val().trim();
		var tagExists = tagsInput.find('input[value="' + tagText + '"]');

		// clear value
		tagInput.val('');
		
		// check if empty
		if (tagText.length <= 0) {
			return;
		}

		// check if tag already exists
		if (tagExists.length > 0) {
			return;
		}

		tagsInput.append('<input name="tag[]" type="hidden" value="' + tagText + '"/>');
		tagsLabel.append('<label class="label label-default tag-label">' + 
							'<span class="tag-text">' + tagText + '</span>' + 
							'&nbsp;&nbsp;<span class="tag-remove">&times;</span>' + 
						 '<label/>');
	});

	$(document).on('click', '.tag-label .tag-remove', function(){
		var _this = $(this);
		var tagLabel = _this.parent();
		var tagLabelText = tagLabel.find('.tag-text');
		var tagInput = tagsInput.find('input[value="' + tagLabelText.html() + '"]');

		tagLabel.remove();
		tagInput.remove();
	});

    /**
	 * Photo Viewer
	 */
    $(document).on('click', '[data-toggle="lightbox"]', function(event) {
        event.preventDefault();
        $(this).ekkoLightbox();
    });

    /**
	 * Comments
	 */
	var commentsDiv = $('#comments');
	var commentsWindow = commentsDiv.find('.window');
	var commentsLoading = commentsDiv.find('.loading');
	var commentsList = commentsWindow.find('.list');
	var commentsGetURL = "{!! route('comments.index', ['id' => $merchant->id, 'type' => 'merchant']) !!}";

	// delete comment
    $("#deleteCommentModal").on("show.bs.modal", function (event) {
    	var _this = $(this);
	    var button = $(event.relatedTarget);
	    var comment = button.data("comment");
	    var deleteURL = "{!! route('comments.destroy', ['comment' => '']) !!}" + '/' + comment;

	    $('#btnDeleteComment').on('click', function(){
	    	$.post(
	    		deleteURL,
	    		{
	    			_token: "{{ csrf_token() }}",
	    			_method: "DELETE",
	    		}, function(result, status) {
	    			$('.comment-' + comment).remove();
	    			_this.modal('hide');
	    		}
	    	);
	    });
	});

	// Get initial comments
	getComments();

	// Get latest comments when scrolled
	commentsWindow.scroll(function() {
		var _this = $(this);
		var bottom = _this[0].scrollHeight - _this[0].scrollTop;

		if (bottom == _this[0].clientHeight) {
			getComments();
		}
	});

	function getComments() {
		// check if comments URL is not null
		if (commentsGetURL == null) {
			return;
		}

		// show loading
		commentsLoading.show();

		// ajax GET to fetch comments
		$.get(
			commentsGetURL, //
			function(result, status){
				var oldestComments = commentsList.html(); 
				var latestComments = result.data;
				var defaultAvatar = "{{ asset('img/user.png') }}";

				// assign next page url on comments URL
				commentsGetURL = result.next_page_url;

				if (oldestComments.length == 0) {
		 			commentsList.html('<p style="text-align:center;">No comments found.</p>');
		 		}

				// check if there is comments
				if (latestComments.length == 0) {
					// hide loading
					commentsLoading.hide();
		 			return;
		 		}		

		 		// loop through latest comments and append it to oldest
		 		$.each(latestComments, function(index, latestComment){
		 			var avatar = latestComment.user.avatar ? latestComment.user.avatar : defaultAvatar;

		 			latestComment.content = formatComment(latestComment.content);

		 			// main comments
		 			var comment = '<li class="comment-' + latestComment.id + '">' +
	 							   		'<img src="' + avatar + '" class="img-circle" alt="Avatar">' +
		 							    '<p>' +
		 							        '<span class="text-primary"><a href="#">' + latestComment.user.firstname + ' ' + latestComment.user.lastname + '</a></span>' + 
		 							             latestComment.content + 
		 							        '<span class="datetime-posted">' + moment(latestComment.created_at, "YYYY-MM-DD H:i:s").fromNow() +
		 							        '<i class="fa fa-trash comment-remove" data-toggle="modal" data-target="#deleteCommentModal" data-comment="' + latestComment.id + '"></i>' +
		 							        '</span>' +
		 							   '</p>';

	 				// main comments all Replies
	 				if (latestComment.all_replies.length > 0) {
	 					var replies = '<ul class="list">';

	 					$.each(latestComment.all_replies, function(index, reply){
	 						var avatar = reply.user.avatar ? reply.user.avatar : defaultAvatar;
	 						var replyTo = '';

	 						// check if reply is a reply to specific reply
	 						if (latestComment.id != reply.parent_id) {
	 							latestComment.all_replies.filter(function(rep){
	 								if (reply.parent_id == rep.id) {
	 									replyTo =  '<a href="#">@' + rep.user.firstname + ' ' + rep.user.lastname + '</a>';
	 									return;
	 								}
		 						});
	 						}

	 						reply.content = formatComment(reply.content);

	 						replies += '<li class="comment-' + reply.id + '">' +
	 							   '<img src="' + avatar + '" class="img-circle" alt="Avatar">' +
	 							   '<p>' +
	 							        '<span class="text-primary"><a href="#">' + reply.user.firstname + ' ' + reply.user.lastname + '</a></span>' + 
	 							        replyTo + ' ' + reply.content + 
	 							        '<span class="datetime-posted">' + moment(reply.created_at, "YYYY-MM-DD H:i:s").fromNow() + 
	 							        '<i class="fa fa-trash comment-remove" data-toggle="modal" data-target="#deleteCommentModal" data-comment="' + reply.id + '"></i>' + 
	 							        '</span>' +  
	 							   '</p>' +
	 							'</li>';
	 					});

	 					comment += replies + '</ul>';
	 				}

	 				oldestComments += comment + '</li>';
		 		});	

		 		commentsList.html(oldestComments);	

		 		// hide loading
				commentsLoading.hide();
			}
		);
	}

	function getFilePathExtension(path) {
		var filename = path.split('\\').pop().split('/').pop();
		return filename.substr(( Math.max(0, filename.lastIndexOf(".")) || Infinity) + 1);
	}

	function formatComment(comment){
		var ext = getFilePathExtension(comment);
		var formattedComment = '';

		switch(ext) {
			case 'jpg':
			case 'jpeg':
			case 'png':
			case 'gif':
				formattedComment = '<img src="' + comment + '" class="img-responsive">';
				break;
			case 'mp3':
			case 'mp4':
				formattedComment = '<audio controls class="audio">' + 
							'<source src="' + comment + '" type="audio/mpeg">' + 
						  '</audio>';
				break;
			case 'ogg':
				formattedComment = '<audio controls class="audio">' + 
							'<source src="' + comment + '" type="audio/ogg">' + 
						  '</audio>';
				break;
			case 'wav':
				formattedComment = '<audio controls class="audio">' + 
							'<source src="' + comment + '" type="audio/wav">' + 
						  '</audio>';
				break;
			default:
				formattedComment = comment;
				break;
		}

		return formattedComment;
	}
</script>
@endpush

<form method="GET" action="{{ url()->current() }}">
	<div class="input-group">
		<span class="input-group-addon">Search</span>
		<input type="text" name="name" id="name" class="form-control" value="{{ old('name') }}" placeholder="Type merchant name">
		<span class="input-group-btn"><button type="submit" class="btn btn-success"><i class="fa fa-search"></i></button></span>
	</div>
</form>


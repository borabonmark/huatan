<form method="POST" action="{{ $action }}" id="merchant-form" enctype="multipart/form-data">
    @csrf
    <input type="hidden" name="_method" value="{{ $method }}">

    <div class="form-group  {{ $errors->has('priority') ? 'has-error has-feedback' : '' }}">
        <div class="checkbox">
            <label for="priority" class="control-label">
                <input type="checkbox" name="priority" id="priority" value="yes" 
                @if('yes' == $input['priority']) checked @endif>
                <strong>{{ __('Set as Priority') }}</strong>
            </label>
        </div>

        @if ($errors->has('priority'))
            <span class="help-block text-left">{{ $errors->first('priority') }}</span>
        @endif
    </div>
    
    <div class="form-group  {{ $errors->has('name') ? 'has-error has-feedback' : '' }}">
        <label for="name" class="control-label">{{ __('Merchant Name') }}</label>
        <input type="text" name="name" id="name" class="form-control" value="{{ $input['name'] }}" required>

        @if ($errors->has('name'))
            <span class="help-block text-left">{{ $errors->first('name') }}</span>
        @endif
    </div>

    <div class="form-group  {{ $errors->has('category_id') ? 'has-error has-feedback' : '' }}">
        <label for="category_id" class="control-label">{{ __('Category') }}</label>
        <select name="category_id" id="category_id" class="form-control" required>
            <option value="">{{ __('Select') }}</option>
            @if(!empty($categories))
                @foreach($categories as $category)
                    <option value="{{ $category->id }}"  
                    @if ($input['category_id'] == $category->id) {{ 'selected' }} @endif>
                        {{ $category->name }}
                    </option>
                @endforeach
            @endif
        </select>

        @if ($errors->has('category_id'))
            <span class="help-block text-left">{{ $errors->first('category_id') }}</span>
        @endif
    </div>

    <div class="form-group  {{ $errors->has('phone') ? 'has-error has-feedback' : '' }}">
        <label for="phone" class="control-label">{{ __('Contact No.') }}</label>
        <input type="text" name="phone" id="phone" class="form-control" value="{{ $input['phone'] }}" required>

        @if ($errors->has('phone'))
            <span class="help-block text-left">{{ $errors->first('phone') }}</span>
        @endif
    </div>

    <div class="form-group  {{ $errors->has('email') ? 'has-error has-feedback' : '' }}">
        <label for="email" class="control-label">{{ __('Email Address') }}</label>
        <input type="email" name="email" id="email" class="form-control" value="{{ $input['email'] }}" required>

        @if ($errors->has('email'))
            <span class="help-block text-left">{{ $errors->first('email') }}</span>
        @endif
    </div>

    <div class="form-group  {{ $errors->has('address') ? 'has-error has-feedback' : '' }}">
        <label for="address" class="control-label">{{ __('Complete Address') }}</label>
        <textarea name="address" id="address" class="form-control" required>{{ $input['address'] }}</textarea>

        @if ($errors->has('address'))
            <span class="help-block text-left">{{ $errors->first('address') }}</span>
        @endif
    </div>

    <div class="form-group  {{ $errors->has('description') ? 'has-error has-feedback' : '' }}">
        <label for="description" class="control-label">{{ __('Brief Description') }}</label>
        <textarea name="description" id="description" class="form-control" rows="5" required>{{ $input['description'] }}</textarea>

        @if ($errors->has('description'))
            <span class="help-block text-left">{{ $errors->first('description') }}</span>
        @endif
    </div>

    <div class="form-group  {{ $errors->has('activity') ? 'has-error has-feedback' : '' }}">
        <label for="activity" class="control-label">{{ __('Latest Activity') }}</label>
        <textarea name="activity" id="activity" class="form-control" required>{{ $input['activity'] }}</textarea>

        @if ($errors->has('activity'))
            <span class="help-block text-left">{{ $errors->first('activity') }}</span>
        @endif
    </div>

    <div class="form-group  {{ $errors->has('logo') ? 'has-error has-feedback' : '' }}">
        <label for="logo" class="control-label">{{ __('Logo') }}</label>
        <input type="file" name="logo" id="logo" class="form-control">
        <p class="help-block">{{ __('Must be an image (jpeg, png, bmp, gif). Dimension atleast') }} {{ $logo['width' ]}}x{{ $logo['height'] }} {{ __('pixels and maximum file size') }} {{ $logo['size'] }}MB </p>

        @if ($errors->has('logo'))
            <span class="help-block text-left">{{ $errors->first('logo') }}</span>
        @endif
    </div>

    <div class="form-group  {{ $errors->has('longitude') ? 'has-error has-feedback' : '' }}">
        <label for="longitude" class="control-label">{{ __('Longitude') }}</label>
        <input type="text" name="longitude" id="longitude" class="form-control" value="{{ $input['longitude'] }}" required>

        @if ($errors->has('longitude'))
            <span class="help-block text-left">{{ $errors->first('longitude') }}</span>
        @endif
    </div>

    <div class="form-group  {{ $errors->has('latitude') ? 'has-error has-feedback' : '' }}">
        <label for="latitude" class="control-label">{{ __('Latitude') }}</label>
        <input type="text" name="latitude" id="latitude" class="form-control" value="{{ $input['latitude'] }}" required>

        @if ($errors->has('latitude'))
            <span class="help-block text-left">{{ $errors->first('latitude') }}</span>
        @endif
    </div>

    <div id="photos-input">
        @if (!empty($input['photos']))
            @foreach($input['photos'] as $photo)
                <input name="photo[]" type="hidden" value="{{ $photo->value }}"/>
            @endforeach
        @endif
        
        @if (!empty($input['thumbnails']))
            @foreach($input['thumbnails'] as $thumbnail)
                <input name="thumbnail[]" type="hidden" value="{{ $thumbnail->value }}"/>
            @endforeach
        @endif
    </div>

    <div id="tags-input">
        @if (!empty($input['tags']))
            @foreach($input['tags'] as $tag)
                <input name="tag[]" type="hidden" value="{{ $tag->name }}"/>
            @endforeach
        @endif
    </div>
</form>

@extends('layouts.app')

@section('content')
	<h3 class="page-title">
		<div class="row">
			<div class="col-md-12">
				<i class="fa fa-building"></i> {{ __('Merchants') }}

				@can('hasAccess', [App\Merchant::class, 'add'])
					<a href="{{ route('merchants.create') }}" class="btn btn-primary">
						<i class="fa fa-plus-square"></i>
					</a>
				@endcan
			</div>
		</div>
	</h3>
	<div class="row">
		<div class="col-md-12">
			<div class="panel">
				<div class="panel-heading">
					@if (session('status'))
				        @component('alert', ['type' => session('status')])
				            {!! session('message') !!}
				        @endcomponent
				    @endif
				</div>

				<div class="panel-body">
					<table id="merchants-table" class="table table-bordered table-hover table-striped">
						<thead>
							<tr>
								<th class="text-center" width="20%">{{ __('Merchant') }} {{__('Name') }}</th>
								<th class="text-center" width="10%">{{ __('Category') }}</th>
								<th class="text-center" width="12%">{{ __('Phone') }}</th>
								<th class="text-center" width="20%">{{ __('Email') }}</th>
								<th class="text-center" width="8%">
									{{ __('Priority') }}
									<button class="btn-update-status" data-statustoupdate="priority">
										<i class="fa fa-check-square"></i>
									</button>
								</th>
								<th class="text-center" width="12%">{{ __('Date Created') }}</th>
								<th class="text-center">{{ __('Actions') }}</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
	</div>
@endsection

@push('html')
<div class="modal fade" id="deleteMerchantModal" tabindex="-1" role="dialog" aria-labelledby="deleteMerchantModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="deleteMerchantModalLabel">{{ __('Delete Merchant') }}</h4>
			</div>
			<div class="modal-body">
				<form method="POST" action="" id="deleteMerchantForm">
                    @csrf
                    @method('DELETE')
                    <p>{{ __('Are you sure you want to delete this merchant?') }}</p>
                    <button type="submit" class="btn btn-danger">{{ __('Delete') }}</button> &nbsp;
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('Cancel') }}</button>
                </form>
			</div>
		</div>
	</div>
</div>
@endpush

@push('css')
<link href="{{ asset('vendor/jquery-datatables/css/jquery.dataTables.min.css') }}" rel="stylesheet">
@endpush

@push('script')
<script src="{{ asset('vendor/jquery-datatables/js/jquery.dataTables.locale.js') }}"></script>
<script>
$(function() {
	/**
	 * List
	 */
    var merchantsTable = $('#merchants-table').DataTable({
    	pageLength: 25,
        processing: true,
        serverSide: true,
        ajax: '{!! route('merchants.getList') !!}',
        columnDefs: [
        	{
        		targets: [4, 5, 6],
        		className: 'dt-body-center dt-head-center'
        	}
        ],
        columns: [
        	{ data: 'name', name: 'name' },
            { data: 'category', name: 'category' },
            { data: 'phone', name: 'phone' },
            { data: 'email', name: 'email' },
            { data: 'priority', name: 'priority', searchable: false, orderable: false },
            { data: 'created_at', name: 'created_at', searchable: false, orderable: false },
            { data: 'action', name: 'action', searchable: false, orderable: false }
        ],

    });

	/**
	 * Delete
	 */
	$("#deleteMerchantModal").on("show.bs.modal", function (event) {
	    var button = $(event.relatedTarget);
	    var url = "{{ url('/') }}/" + Lang.getLocale() + "/merchants/" +  button.data("merchant");
	    $("#deleteMerchantForm").attr("action", url);
	});

	/**
	 * Status Update
	 */
	 $('.btn-update-status').on('click', function(){
	 	var _this = $(this);
	 	var statusToUpdate = _this.data('statustoupdate');
	 	var merchants = [];
	 	var checked = [];

	 	_this.html('<span class="loading"></span>');


	 	$('input[name="' + statusToUpdate + '[]"]').each(function(){
	 		var _this = $(this);

	 		merchants.push(_this.val());

	 		if (_this.is(':checked')) {
	 			checked.push(_this.val());
	 		}
	 	});

	 	$.post(
	 		"{!! route('merchants.statusUpdate') !!}", 
	 		{
	 			_token: "{{ csrf_token() }}",
	 			statusToUpdate: statusToUpdate,
	 			merchants: merchants,
	 			checked: checked
	 		}, 
	 		function(result, status){
				$('.panel-heading').html(
					'<div class="alert alert-' + result.status + ' alert-dismissible" role="alert">' +
						'<button type="button" class="close" data-dismiss="alert" aria-label="Close">' + 
							'<span aria-hidden="true">×</span>' + 
						'</button>' +
						'<i class="fa fa-check-circle"></i> ' + result.message + 
					'</div>'
			    );

			    _this.html('<i class="fa fa-check-square"></i>');

			    setTimeout(function(){ $('.panel-heading').html(''); }, 3000);
	 		}
	 	);
	});
});
</script>
@endpush
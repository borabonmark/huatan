@extends('layouts.app')

@section('content')
	<h3 class="page-title">
		<div class="row">
			<div class="col-md-12">
				<i class="fa fa-users"></i> {{ __($role ? ucfirst($role) : 'Users') }}
				@can('hasAccess', [App\User::class, 'add'])
					<a href="{{ route('users.create') }}" class="btn btn-primary">
						<i class="fa fa-plus-square"></i>
					</a>
				@endcan
			</div>
		</div>
	</h3>
	<div class="row">
		<div class="col-md-12">			
			<div class="panel">
				<div class="panel-heading">
					@if (session('status'))
				        @component('alert', ['type' => session('status')])
				            {!! session('message') !!}
				        @endcomponent
				    @endif
				</div>

				<div class="panel-body">
					<table id="users-table" class="table table-bordered table-hover table-striped">
						<thead>
							<tr>
								<th>{{ __('Name') }}</th>
								<th>{{ __('Email') }}</th>
								<th>{{ __('Phone') }}</th>
								<th width="8%">{{ __('Role') }}</th>
								<th width="8%">
									{{ __('Active') }}
									<button class="btn-update-status" data-statustoupdate="status">
										<i class="fa fa-check-square"></i>
									</button>
								</th>
								<th width="8%">{{ __('Actions') }}</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
	</div>
@endsection

@push('html')
<div class="modal fade" id="deleteUserModal" tabindex="-1" role="dialog" aria-labelledby="deleteUserModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="deleteUserModalLabel">{{ __('Delete User') }}</h4>
			</div>
			<div class="modal-body">
				<form method="POST" action="" id="deleteUserForm">
                    @csrf
                    @method('DELETE')
                    <p>{{ __('Are you sure you want to delete this user?') }}</p>
                    <button type="submit" class="btn btn-danger">{{ __('Delete') }}</button> &nbsp;
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('Cancel') }}</button>
                </form>
			</div>
		</div>
	</div>
</div>
@endpush

@push('css')
<link href="{{ asset('vendor/jquery-datatables/css/jquery.dataTables.min.css') }}" rel="stylesheet">
@endpush

@push('script')
<script src="{{ asset('vendor/jquery-datatables/js/jquery.dataTables.locale.js') }}"></script>
<script>

$(function() {

	/**
	 * List
	 */
    var usersTable = $('#users-table').DataTable({
    	pageLength: 25,
        processing: true,
        serverSide: true,
        ajax: '{!! route('users.getList', ['role' => $role ? $role : 'user']) !!}',
        columnDefs: [
        	{
        		targets: [3, 4, 5],
        		className: 'dt-body-center dt-head-center'
        	}
        ],
        columns: [
        	{ data: 'fullname', name: 'fullname' },
            { data: 'email', name: 'email' },
            { data: 'phone', name: 'phone' },
            { data: 'role', name: 'role', searchable: false, orderable: false },
            { data: 'status', name: 'status', searchable: false, orderable: false },
            { data: 'action', name: 'action', searchable: false, orderable: false }
        ],

    });

    /**
	 * Delete User
	 */
	$("#deleteUserModal").on("show.bs.modal", function (event) {
	    var button = $(event.relatedTarget);
	    var url = "{{ url('/') }}/" + Lang.getLocale() + "/users/" +  button.data("user");
	    $("#deleteUserForm").attr("action", url);
	    usersTable.draw(false);
	});

	/**
	 * Status Update
	 */
	 $('.btn-update-status').on('click', function(){
	 	var _this = $(this);
	 	var statusToUpdate = _this.data('statustoupdate');
	 	var users = [];
	 	var checked = [];

	 	_this.html('<span class="loading"></span>');


	 	$('input[name="' + statusToUpdate + '[]"]').each(function(){
	 		var _this = $(this);

	 		users.push(_this.val());

	 		if (_this.is(':checked')) {
	 			checked.push(_this.val());
	 		}
	 	});

	 	$.post(
	 		"{!! route('users.statusUpdate') !!}", 
	 		{
	 			_token: "{{ csrf_token() }}",
	 			statusToUpdate: statusToUpdate,
	 			users: users,
	 			checked: checked
	 		}, 
	 		function(result, status){
				$('.panel-heading').html(
					'<div class="alert alert-' + result.status + ' alert-dismissible" role="alert">' +
						'<button type="button" class="close" data-dismiss="alert" aria-label="Close">' + 
							'<span aria-hidden="true">×</span>' + 
						'</button>' +
						'<i class="fa fa-check-circle"></i> ' + result.message + 
					'</div>'
			    );

			    _this.html('<i class="fa fa-check-square"></i>');

			    usersTable.draw(false);

			    setTimeout(function(){ $('.panel-heading').html(''); }, 3000);
	 		}
	 	);
	});
});
</script>
@endpush
@extends('layouts.app')

@section('content')
	<h3 class="page-title">
		<i class="fa fa-user"></i> {{ __('Edit User') }}

		@can('hasAccess', [App\User::class, 'add'])
			<a href="{{ route('users.create') }}" class="btn btn-primary">
				<i class="fa fa-plus-square"></i>
			</a>
		@endcan
	</h3>
	<div class="row">
		<div class="col-md-7">
			<div class="panel">
				<div class="panel-heading">
					<h3 class="panel-title">
						<strong><i class="fa fa-file"></i> {{ __('User Information') }}</strong>
					</h3>
				</div>
				<div class="panel-body">
					@if (session('status') && session('status_for') && ('user_info' == session('status_for'))) 
				        @component('alert', ['type' => session('status')])
				            {!! session('message') !!}
				        @endcomponent
				    @endif

					@include('users.forms.edit')
				</div>
			</div>
			<div class="panel">
				<div class="panel-heading">
					<h3 class="panel-title">
						<strong><i class="fa fa-key"></i> {{ __('Change Password') }}</strong>
					</h3>
				</div>
				<div class="panel-body">
					@if (session('status') && session('status_for') && ('change_password' == session('status_for'))) 
				        @component('alert', ['type' => session('status')])
				            {!! session('message') !!}
				        @endcomponent
				    @endif

					@include('users.forms.change_password')
				</div>
			</div>
		</div>
		<div class="col-md-5">
			@if ($user->hasRole('admin'))
				<div class="panel">
					<div class="panel-heading">
						<h3 class="panel-title">
							<strong><i class="fa fa-users"></i> {{ __('Modules Access') }}</strong>
						</h3>
					</div>
					<div class="panel-body">
						@if (session('status') && session('status_for') && ('modules_access' == session('status_for'))) 
					        @component('alert', ['type' => session('status')])
					            {!! session('message') !!}
					        @endcomponent
					    @endif
						@include('modules.form')
					</div>
				</div>
			@endif
		</div>
	</div>
@endsection
<form method="POST" action="{{ route('users.changepassword', ['user' => $user->id]) }}">
    @csrf

    <div class="form-group  {{ $errors->has('password') ? 'has-error has-feedback' : '' }}">
        <label for="password" class="control-label">{{ __('New Password') }}</label>
        <input type="password" name="password" id="password" class="form-control" required>
        <p class="help-block">{{ __('Must be at least 6 characters.') }}</p>

        @if ($errors->has('password'))
            <span class="help-block text-left">{{ $errors->first('password') }}</span>
        @endif
    </div>

    <div class="form-group  {{ $errors->has('password_confirmation') ? 'has-error has-feedback' : '' }}">
        <label for="password_confirmation" class="control-label">{{ __('Confirm New Password') }}</label>
        <input type="password" name="password_confirmation" id="password_confirmation" class="form-control" required>

        @if ($errors->has('password_confirmation'))
            <span class="help-block text-left">{{ $errors->first('password_confirmation') }}</span>
        @endif
    </div>

    <div class="form-group">
        <button type="submit" class="btn btn-success">{{ __('Change') }}</button>
    </div>
</form>
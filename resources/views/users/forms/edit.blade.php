<form method="POST" action="{{ route('users.update', ['user' => $user->id]) }}">
    @csrf
    @method('PATCH')

    <div class="form-group  {{ $errors->has('firstname') ? 'has-error has-feedback' : '' }}">
        <label for="name" class="control-label">{{ __('First Name') }}</label>
        <input type="text" name="firstname" id="firstname" class="form-control" value="{{ old('firstname', $user->firstname) }}" required>

        @if ($errors->has('firstname'))
            <span class="help-block text-left">{{ $errors->first('firstname') }}</span>
        @endif
    </div>

    <div class="form-group  {{ $errors->has('lastname') ? 'has-error has-feedback' : '' }}">
        <label for="name" class="control-label">{{ __('Last Name') }}</label>
        <input type="text" name="lastname" id="lastname" class="form-control" value="{{ old('lastname', $user->lastname) }}" required>

        @if ($errors->has('lastname'))
            <span class="help-block text-left">{{ $errors->first('lastname') }}</span>
        @endif
    </div>

    <div class="form-group  {{ $errors->has('email') ? 'has-error has-feedback' : '' }}">
        <label for="email" class="control-label">{{ __('Email Address') }}</label>
        <input type="email" name="email" id="email" class="form-control" value="{{ old('email', $user->email) }}" required>

        @if ($errors->has('email'))
            <span class="help-block text-left">{{ $errors->first('email') }}</span>
        @endif
    </div>

    <div class="form-group  {{ $errors->has('phone') ? 'has-error has-feedback' : '' }}">
        <label for="phone" class="control-label">{{ __('Phone') }}</label>
        <input type="phone" name="phone" id="phone" class="form-control" value="{{ old('phone', $user->phone) }}" required>

        @if ($errors->has('phone'))
            <span class="help-block text-left">{{ $errors->first('phone') }}</span>
        @endif
    </div>

    <div class="form-group  {{ $errors->has('status') ? 'has-error has-feedback' : '' }}">
        <label for="status" class="control-label">{{ __('Status') }}</label>
        <select name="status" id="status" class="form-control" required>
            <option value="">Select</option>
            @if(!empty($statuses))
                @foreach($statuses as $status)
                    <option value="{{ $status }}"  
                    @if ($status == $user->status) {{ 'selected' }} @endif>
                        {{ ucfirst($status) }}
                    </option>
                @endforeach
            @endif
        </select>

        @if ($errors->has('status'))
            <span class="help-block text-left">{{ $errors->first('status') }}</span>
        @endif
    </div>

    <div class="form-group">
        <button type="submit" class="btn btn-success">{{ __('Update') }}</button>
        @can('hasAccess', [App\User::class, 'view'])
            <a href="{{ route('users.index') }}" class="btn btn-default">{{ __('Cancel') }}</a>
        @endcan
    </div>
</form>
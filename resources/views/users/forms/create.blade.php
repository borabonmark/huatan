<form method="POST" action="{{ route('users.store') }}">
    @csrf

    <div class="form-group  {{ $errors->has('firstname') ? 'has-error has-feedback' : '' }}">
        <label for="name" class="control-label">{{ __('First Name') }}</label>
        <input type="text" name="firstname" id="firstname" class="form-control" value="{{ old('firstname') }}" required>

        @if ($errors->has('firstname'))
            <span class="help-block text-left">{{ $errors->first('firstname') }}</span>
        @endif
    </div>

    <div class="form-group  {{ $errors->has('lastname') ? 'has-error has-feedback' : '' }}">
        <label for="name" class="control-label">{{ __('Last Name') }}</label>
        <input type="text" name="lastname" id="lastname" class="form-control" value="{{ old('lastname') }}" required>

        @if ($errors->has('lastname'))
            <span class="help-block text-left">{{ $errors->first('lastname') }}</span>
        @endif
    </div>

    <div class="form-group  {{ $errors->has('email') ? 'has-error has-feedback' : '' }}">
        <label for="email" class="control-label">{{ __('Email Address') }}</label>
        <input type="email" name="email" id="email" class="form-control" value="{{ old('email') }}" required>

        @if ($errors->has('email'))
            <span class="help-block text-left">{{ $errors->first('email') }}</span>
        @endif
    </div>

    <div class="form-group  {{ $errors->has('phone') ? 'has-error has-feedback' : '' }}">
        <label for="phone" class="control-label">{{ __('Phone') }}</label>
        <input type="phone" name="phone" id="phone" class="form-control" value="{{ old('phone') }}" required>

        @if ($errors->has('phone'))
            <span class="help-block text-left">{{ $errors->first('phone') }}</span>
        @endif
    </div>

    <div class="form-group  {{ $errors->has('status') ? 'has-error has-feedback' : '' }}">
        <label for="status" class="control-label">{{ __('Status') }}</label>
        <select name="status" id="status" class="form-control" required>
            <option value="">{{ __('Select') }}</option>
            @if(!empty($statuses))
                @foreach($statuses as $status)
                    <option value="{{ $status }}">
                        {{ ucfirst($status) }}
                    </option>
                @endforeach
            @endif
        </select>

        @if ($errors->has('status'))
            <span class="help-block text-left">{{ $errors->first('status') }}</span>
        @endif
    </div>

    <div class="form-group  {{ $errors->has('password') ? 'has-error has-feedback' : '' }}">
        <label for="password" class="control-label">{{ __('Password') }}</label>
        <input type="password" name="password" id="password" class="form-control" required>
        <p class="help-block">{{ __('Must be at least 6 characters.') }}</p>

        @if ($errors->has('password'))
            <span class="help-block text-left">{{ $errors->first('password') }}</span>
        @endif
    </div>

    <div class="form-group  {{ $errors->has('password_confirmation') ? 'has-error has-feedback' : '' }}">
        <label for="password_confirmation" class="control-label">{{ __('Confirm Password') }}</label>
        <input type="password" name="password_confirmation" id="password_confirmation" class="form-control" required>

        @if ($errors->has('password_confirmation'))
            <span class="help-block text-left">{{ $errors->first('password_confirmation') }}</span>
        @endif
    </div>

    <div class="form-group">
        <button type="submit" class="btn btn-success">{{ __('Create') }}</button>
        @can('hasAccess', [App\User::class, 'view'])
            <a href="{{ route('users.index') }}" class="btn btn-default">{{ __('Cancel') }}</a>
        @endcan
    </div>
</form>
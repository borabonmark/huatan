@extends('layouts.app')

@section('content')
	<h3 class="page-title">
		<i class="fa fa-user"></i> {{ __('New User') }}
	</h3>
	<div class="row">
		<div class="col-md-7">
			<div class="panel">
				<div class="panel-heading">
					<h3 class="panel-title">
						<strong><i class="fa fa-file"></i> {{ __('User Information') }}</strong>
					</h3>
				</div>
				<div class="panel-body">
					@if (session('status'))
				        @component('alert', ['type' => session('status')])
				            {!! session('message') !!}
				        @endcomponent
				    @endif

					@include('users.forms.create')
				</div>
			</div>
		</div>
	</div>
@endsection

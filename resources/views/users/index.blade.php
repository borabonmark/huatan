@extends('layouts.app')

@section('content')
	<h3 class="page-title">
		<div class="row">
			<div class="col-md-8">
				<i class="fa fa-users"></i> {{ __('Users') }}

				@can('hasAccess', [App\User::class, 'add'])
					<a href="{{ route('users.create') }}" class="btn btn-primary">
						<i class="fa fa-plus-square"></i>
					</a>
				@endcan
			</div>
			<div class="col-md-4">
				@include('users.search')
			</div>
		</div>
	</h3>
	<div class="row">
		<div class="col-md-12">			
			<div class="panel">
				<div class="panel-heading">
					@if (session('status'))
				        @component('alert', ['type' => session('status')])
				            {!! session('message') !!}
				        @endcomponent
				    @endif
				</div>

				<div class="panel-body">
					<table class="table table-bordered table-hover table-striped">
						<thead>
							<tr>
								<th class="text-center" width="10%">{{ __('First Name') }}</th>
								<th class="text-center" width="10%">{{ __('Last Name') }}</th>
								<th class="text-center" width="20%">{{ __('Email') }}</th>
								<th class="text-center" width="10%">{{ __('Role') }}</th>
								<th class="text-center" width="10%">
									{{ __('Active') }}
									<button class="btn-update-status" data-statustoupdate="status">
										<i class="fa fa-check-square"></i>
									</button>
								</th>
								<th class="text-center" width="10%">{{ __('Date Created') }}</th>
								<th class="text-center" width="10%">{{ __('Actions') }}</th>
							</tr>
						</thead>
						<tbody>
							@empty($users->total())
								<tr>
									<td colspan="7" class="text-center">{{ __('No users found.') }}</td>
								</tr>
							@else
								@foreach($users as $user)
									<tr class="text-center">
										<td>{{ $user->firstname }}</td>
										<td>{{ $user->lastname }}</td>
										<td><a href="mailto:{{ $user->email }}">{{ $user->email }}</a></td>
										<td>{{ ucfirst($user->role) }}</td>
										<td>
											<input type="checkbox" name="status[]" value="{{ $user->id }}" @if ('active' == $user->status) checked @endif>
										</td>
										<td>{{ $user->created_at->format('D, M j, Y g:i A') }}</td>
										<td class="text-center">
											@can('hasAccess', [App\User::class, 'edit'])
												<a href="{{ route('users.edit', ['user' => $user->id]) }}">
													<i class="fa fa-pencil"></i> {{ __('Edit') }}
												</a> &nbsp;
											@endcan

											@can('hasAccess', [App\User::class, 'delete'])
												<a href="javascript:void(0);" data-toggle="modal" data-target="#deleteUserModal" data-user="{{ $user->id }}">
		                                            <i class="fa fa-trash"></i> {{ __('Delete') }}
		                                        </a> &nbsp;
	                                        @endcan
										</td>
									</tr>
								@endforeach
							@endempty
						</tbody>
					</table>
					{{ $users->links() }}
				</div>
			</div>
		</div>
	</div>
@endsection

@push('html')
<div class="modal fade" id="deleteUserModal" tabindex="-1" role="dialog" aria-labelledby="deleteUserModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="deleteUserModalLabel">{{ __('Delete User') }}</h4>
			</div>
			<div class="modal-body">
				<form method="POST" action="" id="deleteUserForm">
                    @csrf
                    @method('DELETE')
                    <p>{{ __('Are you sure you want to delete this user?') }}</p>
                    <button type="submit" class="btn btn-danger">{{ __('Delete') }}</button> &nbsp;
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('Cancel') }}</button>
                </form>
			</div>
		</div>
	</div>
</div>
@endpush

@push('script')
<script>
/**
 * Delete User
 */
$("#deleteUserModal").on("show.bs.modal", function (event) {
    var button = $(event.relatedTarget);
    $("#deleteUserForm").attr("action", "{{ url('/users/') }}/" + button.data("user"));
});

/**
 * Status Update
 */
 $('.btn-update-status').on('click', function(){
 	var _this = $(this);
 	var statusToUpdate = _this.data('statustoupdate');
 	var users = [];
 	var checked = [];

 	_this.html('<span class="loading"></span>');


 	$('input[name="' + statusToUpdate + '[]"]').each(function(){
 		var _this = $(this);

 		users.push(_this.val());

 		if (_this.is(':checked')) {
 			checked.push(_this.val());
 		}
 	});

 	$.post(
 		"{!! route('users.statusUpdate') !!}", 
 		{
 			_token: "{{ csrf_token() }}",
 			statusToUpdate: statusToUpdate,
 			users: users,
 			checked: checked
 		}, 
 		function(result, status){
			$('.panel-heading').html(
				'<div class="alert alert-' + result.status + ' alert-dismissible" role="alert">' +
					'<button type="button" class="close" data-dismiss="alert" aria-label="Close">' + 
						'<span aria-hidden="true">×</span>' + 
					'</button>' +
					'<i class="fa fa-check-circle"></i> ' + result.message + 
				'</div>'
		    );

		    _this.html('<i class="fa fa-check-square"></i>');

		    setTimeout(function(){ $('.panel-heading').html(''); }, 3000);
 		}
 	);
 });
</script>
@endpush
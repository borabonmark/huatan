<div id="sidebar-nav" class="sidebar">
	<div class="sidebar-scroll">
		<nav>
			<ul class="nav">
				<li><a href="{{ route('home') }}" class="@if(url()->current() == route('home')) active @endif">
					<i class="fa fa-home"></i> <span>{{ __('Dashboard') }}</span></a>
				</li>

				@can('hasAccess', [App\Merchant::class, 'view'])
					<li><a href="{{ route('merchants.list') }}" class="@if(url()->current() == route('merchants.list')) active @endif">
						<i class="fa fa-building"></i> <span>{{ __('Merchants') }}</span></a>
					</li>
				@endcan

				@can('hasAccess', [App\Advertisement::class, 'view'])
					<li><a href="{{ route('advertisements.list') }}" class="@if(url()->current() == route('advertisements.list')) active @endif"><i class="fa fa-tv"></i> <span>{{ __('Advertisements') }}</span></a></li>
				@endcan

				@can('hasAccess', [App\Article::class, 'view'])
					<li><a href="{{ route('articles.list') }}" class="@if(url()->current() == route('articles.list')) active @endif"><i class="fa fa-file"></i> <span>{{ __('News') }}</span></a></li>
				@endcan

				@can('hasAccess', [App\User::class, 'view'])
					<li>
						<a href="#userSubPages" data-toggle="collapse" class="@if((url()->current() == route('users.list')) || url()->current() == route('users.list', ['role' => 'admin'])) active @else collapsed @endif">
							<i class="fa fa-users"></i> <span>{{ __('Users') }}</span> 
							<i class="icon-submenu lnr lnr-chevron-left"></i>
						</a>
						<div id="userSubPages" class="collapse @if((url()->current() == route('users.list')) || url()->current() == route('users.list', ['role' => 'admin'])) in @endif">
							<ul class="nav">
								<li><a href="{{ route('users.list') }}" class="">{{ __('User') }}</a></li>
								<li><a href="{{ route('users.list', ['role' => 'admin']) }}" class="">{{ __('Admin') }}</a></li>
							</ul>
						</div>
					</li>
				@endcan
			</ul>
		</nav>
	</div>
</div>
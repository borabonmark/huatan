<nav class="navbar navbar-default navbar-fixed-top">
	<div class="brand">
		<a href="{{ route('home') }}"><img src="{{ asset('img/logo.png') }}" alt="Klorofil Logo" class="img-responsive logo"></a>
	</div>
	<div class="container-fluid">
		<div id="navbar-menu">
			<ul class="nav navbar-nav navbar-right">
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<span>{{ LaravelLocalization::getCurrentLocaleName() }}</span> 
						<i class="icon-submenu lnr lnr-chevron-down"></i>
					</a>
					<ul class="dropdown-menu">
						@include('partials.locales')
					</ul>
				</li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="{{ Auth::user()->avatar ? Auth::user()->avatar : asset('img/user.png') }}" class="img-circle" alt="Avatar"> <span>{{ Auth::user()->firstname }}</span> <i class="icon-submenu lnr lnr-chevron-down"></i></a>
					<ul class="dropdown-menu">
						<li>
							<a href="{{ route('account.settings') }}">
                        		<i class="fa fa-cog"></i> <span>{{ __('Account Settings') }}</span>
                        	</a>
                        </li>
						<li>
							<a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logoutForm').submit();">
                        		<i class="fa fa-sign-out"></i> <span>{{ __('Logout') }}</span>
                        	</a>
                        </li>
					</ul>

					<form method="POST" action="{{ route('logout') }}" id="logoutForm" style="display: none;">
                        @csrf
                    </form>
				</li>
			</ul>
		</div>
	</div>
</nav>
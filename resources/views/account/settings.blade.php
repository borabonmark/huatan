@extends('layouts.app')

@section('content')
	<h3 class="page-title">
		<i class="fa fa-cog"></i> {{ __('Account Settings') }}
	</h3>
	<div class="row">
		<div class="col-md-7">
			<div class="panel">
				<div class="panel-heading">
					<h3 class="panel-title">
						<strong><i class="fa fa-file"></i> {{ __('My Information') }}</strong>
					</h3>
				</div>
				<div class="panel-body">
					@if (session('status') && session('status_for') && ('update' == session('status_for'))) 
				        @component('alert', ['type' => session('status')])
				            {!! session('message') !!}
				        @endcomponent
				    @endif

					@include('account.forms.edit')
				</div>
			</div>
			<div class="panel">
				<div class="panel-heading">
					<h3 class="panel-title">
						<strong><i class="fa fa-key"></i> {{ __('Change Password') }}</strong>
					</h3>
				</div>
				<div class="panel-body">
					@if (session('status') && session('status_for') && ('change_password' == session('status_for'))) 
				        @component('alert', ['type' => session('status')])
				            {!! session('message') !!}
				        @endcomponent
				    @endif

					@include('account.forms.change_password')
				</div>
			</div>
		</div>
		<div class="col-md-5"></div>
	</div>
@endsection
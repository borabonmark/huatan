<form method="POST" action="{{ route('account.update') }}">
    @csrf
    @method('PATCH')

    <div class="form-group  {{ $errors->has('firstname') ? 'has-error has-feedback' : '' }}">
        <label for="name" class="control-label">{{ __('First Name') }}</label>
        <input type="text" name="firstname" id="firstname" class="form-control" value="{{ old('firstname', $user->firstname) }}" required>

        @if ($errors->has('firstname'))
            <span class="help-block text-left">{{ $errors->first('firstname') }}</span>
        @endif
    </div>

    <div class="form-group  {{ $errors->has('lastname') ? 'has-error has-feedback' : '' }}">
        <label for="name" class="control-label">{{ __('Last Name') }}</label>
        <input type="text" name="lastname" id="lastname" class="form-control" value="{{ old('lastname', $user->lastname) }}" required>

        @if ($errors->has('lastname'))
            <span class="help-block text-left">{{ $errors->first('lastname') }}</span>
        @endif
    </div>

    <div class="form-group  {{ $errors->has('email') ? 'has-error has-feedback' : '' }}">
        <label for="email" class="control-label">{{ __('Email Address') }}</label>
        <input type="email" name="email" id="email" class="form-control" value="{{ old('email', $user->email) }}" required>

        @if ($errors->has('email'))
            <span class="help-block text-left">{{ $errors->first('email') }}</span>
        @endif
    </div>

    <div class="form-group">
        <button type="submit" class="btn btn-success">{{ __('Update') }}</button>
    </div>
</form>
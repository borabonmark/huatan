<form method="POST" action="{{ route('users.modules', ['user' => $user->id]) }}">
	@csrf

	<div class="form-group">
		<div class="row">
			@foreach($modules as $module)
				<div class="col-md-4">
					<strong>{{ __($module->name) }}</strong>

					@foreach($actions as $action)
						<label class="fancy-checkbox">
							<input type="checkbox" name="modules_access[{{ $module->id }}][]" value="{{ $action }}"
							@if (in_array($action, $user->access($module))) {{ 'checked' }}@endif >
							<span>{{ __(ucfirst($action)) }}</span>
						</label>				
					@endforeach
				</div>
			@endforeach
		</div>
	</div>
	<div class="form-group">
        <button type="submit" class="btn btn-success">{{ __('Save') }}</button>
    </div>
</form>
<?php

namespace App;

use App\PostMeta;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'content'
    ];

    /**
     * Get meta
     */
    public function meta()
    {
        return $this->hasMany(PostMeta::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getMetaByKey($key)
    {
        return $this->meta->where('key', $key)->pluck('value')->first();
    }
}

<?php

namespace App;

use App\Comment;
use Illuminate\Database\Eloquent\Model;

class CommentMeta extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'comment_meta';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'comment_id', 'name', 'key', 'value'
    ];

    /**
     * Get the article
     */
    public function comment()
    {
        return $this->belongsTo(Comment::class);
    }
}

<?php

namespace App;

use App\Post;
use Illuminate\Database\Eloquent\Model;

class PostMeta extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'post_meta';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'post_id', 'name', 'key', 'value'
    ];

    /**
     * Get the article
     */
    public function post()
    {
        return $this->belongsTo(Post::class);
    }
}

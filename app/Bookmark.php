<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Bookmark extends Model
{
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'user_id', 'bookmarkable_id', 'bookmarkable_type'
    ];

    /**
     * Get all of the owning bookmarkable models.
     */
    public function bookmarkable()
    {
        return $this->morphTo();
    }

	/**
	 * Get the user
	 */	
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}

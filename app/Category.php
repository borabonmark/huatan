<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'type', 'name'
    ];

    /**
     * Types
     */
    const TYPE_BUSINESS = 'business';
    const TYPE_NEWS = 'news';

    public static $types = [
        self::TYPE_BUSINESS,
        self::TYPE_NEWS
    ];
}

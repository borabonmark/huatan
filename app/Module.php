<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * List of statuses
     */
    const ACTION_VIEW = 'view';
    const ACTION_ADD = 'add';
    const ACTION_EDIT = 'edit';
    const ACTION_DELETE = 'delete';

    public static $actions = [
        self::ACTION_VIEW,
        self::ACTION_ADD,
        self::ACTION_EDIT,
        self::ACTION_DELETE
    ];

    /**
     * Get modules
     */
    public function users()
    {
        return $this->belongsToMany(User::class)
                    ->withPivot('action')
                    ->withTimestamps();
    }
}

<?php

namespace App\Policies;

use App\User;
use App\Module;
use App\Article;
use Illuminate\Auth\Access\HandlesAuthorization;

class ArticlePolicy
{
    use HandlesAuthorization;

    protected $module;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->module = Module::where('name', 'Article')->first();
    }

    /**
     * Check if user has access
     *
     * @param  \App\User  $user
     * @param  string  $action (view, add, edit, delete)
     * @return bool
     */
    public function hasAccess(User $user, $action)
    {
        return in_array($action, $user->access($this->module));
    }

    /**
     * Check if article belongs to user
     *
     * @param  \App\User  $user
     * @param  \App\Article  $article
     * @return bool
     */
    public function belongsTo(User $user, Article $article)
    {
        return $user->id == $article->user_id;
    }

    /**
     * Check if super admin
     */
    public function before($user, $ability)
    {
        if ($user->hasRole(User::ROLE_SUPER_ADMIN)) {
            return true;
        }
    }
}

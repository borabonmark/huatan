<?php

namespace App;

use App\Tag;
use App\User;
use App\Comment;
use App\Bookmark;
use App\Category;
use Carbon\Carbon;
use App\ArticleMeta;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'category_id', 'title', 'summary', 'content', 'publish', 'date_time_published'
    ];

    protected $dates = ['date_time_published'];

    /**
     * The attributes for photo.
     * 
     * width and height pixels
     * size megabytes
     *
     * @var array
     */
    public static $photo = [
        'width' => 360,
        'height' => 300,
        'size' => 2
    ];

    /**
     * List of statuses
     */
    const PUBLISH_YES = 'yes';
    const PUBLISH_NO = 'no';

    public static $publishStatuses = [
        self::PUBLISH_YES,
        self::PUBLISH_NO
    ];

    /**
     * Get tags
     */
    public function tags()
    {
        return $this->morphToMany(Tag::class, 'taggable');
    }

    /**
     * Get the author
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get the category
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * Get meta
     */
    public function meta()
    {
        return $this->hasMany(ArticleMeta::class);
    }

    /**
     * Get specific meta data value
     */
    public function getMetaByKey($key)
    {
        return $this->meta->where('key', $key)->pluck('value')->first();
    }

    /**
     * Get photos
     */
    public function photos($limit = 0)
    {
        if (1 == $limit) {
            return $this->meta()->where('key', 'photo')->first();
        } elseif ($limit > 1) {
            return $this->meta()->where('key', 'photo')->get()->take($limit);
        } else {
            return $this->meta()->where('key', 'photo')->get();
        }
    }

    /**
     * Get thumbnails
     */
    public function thumbnails($limit = 0)
    {
        if (1 == $limit) {
            return $this->meta()->where('key', 'thumbnail')->first();
        } elseif ($limit > 1) {
            return $this->meta()->where('key', 'thumbnail')->get()->take($limit);
        } else {
            return $this->meta()->where('key', 'thumbnail')->get();
        }
    }

    /**
     * Get main comments
     */
    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable')->whereNull('parent_id');
    }

    /**
     * Get all comments and replies
     */
    public function totalComments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }


    /**
     * Get all bookmarks
     */
    public function bookmarks()
    {
        return $this->morphMany(Bookmark::class, 'bookmarkable');
    }
}

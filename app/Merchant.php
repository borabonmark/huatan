<?php

namespace App;

use App\Tag;
use App\Comment;
use App\Bookmark;
use App\Category;
use Carbon\Carbon;
use App\MerchantMeta;
use Illuminate\Database\Eloquent\Model;

class Merchant extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'category_id', 'name', 'email', 'phone', 
        'address', 'description', 'activity', 'logo'
    ];

    /**
     * The attributes for logo.
     * 
     * width and height pixels
     * size megabytes
     *
     * @var array
     */
    public static $logo = [
        'width' => 300,
        'height' => 300,
        'size' => 2
    ];

    /**
     * The attributes for photo.
     * 
     * width and height pixels
     * size megabytes
     *
     * @var array
     */
    public static $photo = [
        'width' => 360,
        'height' => 300,
        'size' => 2
    ];

    /**
     * Get tags
     */
    public function tags()
    {
        return $this->morphToMany(Tag::class, 'taggable');
    }

    /**
     * Get the category
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * Get meta
     */
    public function meta()
    {
        return $this->hasMany(MerchantMeta::class);
    }

    /**
     * Get specific meta data value
     */
    public function getMetaByKey($key)
    {
        return $this->meta->where('key', $key)->pluck('value')->first();
    }

    /**
     * Get photos
     */
    public function photos($limit = 0)
    {
        if (1 == $limit) {
            return $this->meta()->where('key', 'photo')->first();
        } elseif ($limit > 1) {
            return $this->meta()->where('key', 'photo')->get()->take($limit);
        } else {
            return $this->meta()->where('key', 'photo')->get();
        }
    }

    /**
     * Get thumbnails
     */
    public function thumbnails($limit = 0)
    {
        if (1 == $limit) {
            return $this->meta()->where('key', 'thumbnail')->first();
        } elseif ($limit > 1) {
            return $this->meta()->where('key', 'thumbnail')->get()->take($limit);
        } else {
            return $this->meta()->where('key', 'thumbnail')->get();
        }
    }

     /**
     * Get main comments
     */
    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    /**
     * Get all comments and replies
     */
    public function totalComments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    /**
     * Get all bookmarks
     */
    public function bookmarks()
    {
        return $this->morphMany(Bookmark::class, 'bookmarkable');
    }
}

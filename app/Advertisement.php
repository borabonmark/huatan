<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Advertisement extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'title', 'url', 'status', 'date_time_online', 'material'
    ];

    protected $dates = ['date_time_online'];

    /**
     * The attributes for material.
     * 
     * width and height pixels
     * size megabytes
     *
     * @var array
     */
    public static $material = [
        'width' => 360,
        'height' => 300,
        'size' => 2
    ];

    /**
     * List of statuses
     */
    const STATUS_ONLINE = 'online';
    const STATUS_OFFLINE = 'offline';

    public static $statuses = [
        self::STATUS_ONLINE,
        self::STATUS_OFFLINE
    ];
}

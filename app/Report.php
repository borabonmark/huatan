<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'user_id', 'reportable_id', 'reportable_type', 'content'
    ];

    public function reportable()
    {
        return $this->morphTo();
    }

	/**
	 * Get the user
	 */	
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}

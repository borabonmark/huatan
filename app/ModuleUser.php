<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModuleUser extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'module_user';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'module_id'];
}

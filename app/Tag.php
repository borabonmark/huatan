<?php

namespace App;

use App\Article;
use App\Merchant;
use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    /**
     * Get merchants
     */
    public function merchants()
    {
        return $this->morphedByMany(Merchant::class, 'taggable');
    }

    /**
     * Get articles
     */
    public function articles()
    {
        return $this->morphedByMany(Article::class, 'taggable');
    }
}

<?php

namespace App\Http\Controllers;

use App\User;
use App\UserMeta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AccountController extends Controller
{
    /**
     * Settings
     *
     * @return \Illuminate\Http\Response
     */
    public function settings()
    {
        $user = Auth::user();
        return view('account.settings', compact('user'));
    }

    /**
     * Update
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        Validator::make($request->all(), [
            'firstname' => 'required|string|max:255',
            'lastname' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users,email,' . Auth::id()
        ])->validate();

        Auth::user()->fill($request->only(['email']))->save();

        UserMeta::whereIn('key', ['firstname', 'lastname'])->where('user_id', Auth::id())->delete();
        Auth::user()->meta()->createMany([
            ['name' => 'FirstName', 'key' => 'firstname', 'value' =>  $request->firstname],
            ['name' => 'LastName', 'key' => 'lastname', 'value' =>  $request->lastname]
        ]);

        return back()->with('status_for', 'update')
        			 ->with('status', 'success')
                     ->with('message', '<i class="fa fa-check-circle"></i> ' . __('Your information has been updated.'));
    }

    /**
     * Change Password
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function changePassword(Request $request)
    {
    	$validator = Validator::make($request->all(), [
    		'current_password' => 'required',
            'password' => 'required|string|min:6|confirmed'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

    	// Check if entered current password matches the current password
        if (!(Hash::check($request->current_password, Auth::user()->password))) {
            $validator->getMessageBag()->add('current_password', __('Invalid current password.'));
            return redirect()->back()->withErrors($validator);
        }

        // Check if current and new password are the same
        if (strcmp($request->current_password, $request->password) == 0) {
            $validator->getMessageBag()->add('password', __('New password cannot be same as your current password.'));
            return redirect()->back()->withErrors($validator);
        }

        Auth::user()->password = Hash::make($request->password);
        Auth::user()->save();

        return back()->with('status_for', 'change_password')
         			 ->with('status', 'success')
                     ->with('message', '<i class="fa fa-check-circle"></i> ' . __('Your password has been changed.'));
    }
}

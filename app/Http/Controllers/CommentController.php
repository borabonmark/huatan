<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    /**
     * Pagination max no. of items per page
     */
    public $itemsPerPage = 20;

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $comments = Comment::where('commentable_id', $request->id)
                           ->where('commentable_type', 'App\\' . $request->type)
                           ->where('parent_id', NULL)
                           ->with('user')
                           ->with(['allReplies' => function($query) use ($request){
                                $query->where('parent_id', '<>', NULL)
                                      ->where('commentable_type', 'App\\' . $request->type)
                                      ->with('user');
                           }])
                           ->paginate($this->itemsPerPage);

        // To add get request on pagination
        $comments->appends($request->all());

        return response()->json($comments);
    }

    /**
     * Delete
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Comment $comment)
    {
        $comment->delete();
        return response()->json(['message' => 'success']);
    }

}

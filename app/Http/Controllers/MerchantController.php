<?php

namespace App\Http\Controllers;

use App\Tag;
use DataTables;
use App\Taggable;
use App\Merchant;
use App\Category;
use App\MerchantMeta;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Traits\CustomImageUpload;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class MerchantController extends Controller
{
    use CustomImageUpload;

    /**
     * Pagination max no. of items per page
     */
    public $itemsPerPage = 20;

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('hasAccess', [Merchant::class, 'view']);

        $merchants = Merchant::orderBy('id', 'desc')
                             ->where(function ($query) use ($request) {
                                // Search by name
                                if (!empty($request->name)) {
                                    $query->where('name', 'LIKE','%'. $request->name .'%');
                                }
                             })->paginate($this->itemsPerPage);

        // To add search on pagination
        $merchants->appends($request->all());

        // To repopulate search input
        $request->flash();

        return view('merchants.index', compact('merchants'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('hasAccess', [Merchant::class, 'add']);

        $categories = Category::where('type', Category::TYPE_BUSINESS)->get();
        $logo = Merchant::$logo; 
        $photo = Merchant::$photo;

        return view('merchants.create', compact('categories', 'logo', 'photo'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->only([
            'name', 'category_id', 'phone', 'email', 'address', 'description', 'activity', 'logo'
        ]);

        $this->validator($data)->validate();
        
        $data['logo'] = $this->customImageUpload($request->logo, 'images', 'gcs', 
                        Merchant::$logo['width'], Merchant::$logo['height']);

        $merchant = Merchant::create($data);

        if (!empty($request->has('priority'))) {
            $merchant->meta()->save(new MerchantMeta(['name' => 'Priority', 'key' => 'priority', 'value' => 'yes']));
        }

        if (!empty($request->has('longitude'))) {
            $merchant->meta()->save(new MerchantMeta(['name' => 'Longitude', 'key' => 'longitude', 'value' => (float) $request->longitude]));
        }

        if (!empty($request->has('latitude'))) {
            $merchant->meta()->save(new MerchantMeta(['name' => 'Latitude', 'key' => 'latitude', 'value' => (float) $request->latitude]));
        }

        if (!empty($request->has('photo'))) {
            foreach($request->photo as $photo) {
                $merchant->meta()->save(new MerchantMeta(['name' => 'Photo', 'key' => 'photo', 'value' => $photo]));
            }
        }

        if (!empty($request->has('thumbnail'))) {
            foreach($request->thumbnail as $thumbnail) {
                $merchant->meta()->save(new MerchantMeta(['name' => 'Thumbnail', 'key' => 'thumbnail', 'value' => $thumbnail]));
            }
        }

        if (!empty($request->has('tag'))) {
            foreach($request->tag as $tag) {
                $tag = Tag::firstOrCreate(['name' => $tag]);

                Taggable::create([
                    'tag_id' => $tag->id,
                    'taggable_id' => $merchant->id,
                    'taggable_type' => Merchant::class
                ]);
            }
        }

        if (!Auth::user()->can('hasAccess', [Merchant::class, 'edit'])) {
            return back()->with('status', 'success')
                         ->with('message', '<i class="fa fa-check-circle"></i> ' . __('Merchant has been saved.'));
        }

        return redirect()->route('merchants.edit', ['merchant' => $merchant->id])
                         ->with('status', 'success')
                         ->with('message', '<i class="fa fa-check-circle"></i> ' . __('Merchant has been saved.'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Merchant  $merchant
     * @return \Illuminate\Http\Response
     */
    public function show(Merchant $merchant)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Merchant  $merchant
     * @return \Illuminate\Http\Response
     */
    public function edit(Merchant $merchant)
    {
        $this->authorize('hasAccess', [Merchant::class, 'edit']);

        $categories = Category::where('type', Category::TYPE_BUSINESS)->get();
        $logo = Merchant::$logo; 
        $photo = Merchant::$photo;

        return view('merchants.edit', compact('merchant', 'categories', 'logo', 'photo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Merchant  $merchant
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Merchant $merchant)
    {
        $data = $request->only([
            'name', 'category_id', 'phone', 'email', 'address', 'description', 'activity', 'logo'
        ]);

        $this->validator($data, $merchant)->validate();

        if ($request->has('logo')) {
            $data['logo'] = $this->customImageUpload($request->logo, 'images', 'gcs', 
                            Merchant::$logo['width'], Merchant::$logo['height']);
        }

        $merchant->fill($data)->save();

        MerchantMeta::where('key', 'priority')->where('merchant_id', $merchant->id)->delete();
        if (!empty($request->has('priority'))) {
            $merchant->meta()->save(new MerchantMeta(['name' => 'Priority', 'key' => 'priority', 'value' => 'yes']));
        }
        
        if (!empty($request->has('longitude'))) {
            MerchantMeta::where('key', 'longitude')->where('merchant_id', $merchant->id)->delete();
            $merchant->meta()->save(new MerchantMeta(['name' => 'Longitude', 'key' => 'longitude', 'value' => (float) $request->longitude]));
        }

        if (!empty($request->has('latitude'))) {
            MerchantMeta::where('key', 'latitude')->where('merchant_id', $merchant->id)->delete();
            $merchant->meta()->save(new MerchantMeta(['name' => 'Latitude', 'key' => 'latitude', 'value' => (float) $request->latitude]));
        }

        if (!empty($request->has('photo'))) {
            MerchantMeta::where('key', 'photo')->where('merchant_id', $merchant->id)->delete();

            foreach($request->photo as $photo) {
                $merchant->meta()->save(new MerchantMeta(['name' => 'Photo', 'key' => 'photo', 'value' => $photo]));
            }
        }

        if (!empty($request->has('thumbnail'))) {
            MerchantMeta::where('key', 'thumbnail')->where('merchant_id', $merchant->id)->delete();

            foreach($request->thumbnail as $thumbnail) {
                $merchant->meta()->save(new MerchantMeta(['name' => 'Thumbnail', 'key' => 'thumbnail', 'value' => $thumbnail]));
            }
        }

        if (!empty($request->has('tag'))) {
            Taggable::where('taggable_id', $merchant->id)
                    ->where('taggable_type', Merchant::class)
                    ->delete();

            foreach($request->tag as $tag) {
                $tag = Tag::firstOrCreate(['name' => $tag]);

                Taggable::create([
                    'tag_id' => $tag->id,
                    'taggable_id' => $merchant->id,
                    'taggable_type' => Merchant::class
                ]);
            }
        }

        return back()->with('status', 'success')
                     ->with('message', '<i class="fa fa-check-circle"></i> ' . __('Merchant has been updated.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Merchant  $merchant
     * @return \Illuminate\Http\Response
     */
    public function destroy(Merchant $merchant)
    {
        $this->authorize('hasAccess', [Merchant::class, 'delete']);

        $merchant->delete();
        return back()->with('status', 'success')
                     ->with('message', '<i class="fa fa-check-circle"></i> ' . __('Merchant has been deleted.'));
    }

    /**
     * Create validator
     *
     * @param  array  $data
     * @param  \App\Merchant  $merchant
     * @return \Illuminate\Http\Response
     */
    protected function validator(array $data, $merchant = NULL)
    {
        $width = Merchant::$logo['width'];
        $height = Merchant::$logo['height'];
        $size = Merchant::$logo['size'] * 1024;

        $rules = [
            'name' => 'required|string|max:255',
            'category_id' => 'required|exists:categories,id',
            'phone' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:merchants',
            'address' => 'required|string',
            'description' => 'required|string',
            'activity' => 'required|string',
            'logo' => [
                'required', 
                'image',
                'max:' . $size,
                Rule::dimensions()->minWidth($width)
                                  ->minHeight($height)
            ]
        ];

        if ($merchant) {
            $rules['email'] = 'required|string|email|max:255|unique:merchants,email,' . $merchant->id;
            unset($rules['logo']['required']);
            array_unshift($rules['logo'], 'sometimes');
        }

        return Validator::make($data, $rules);
    }

    /**
     * Update merchant priority etc. status 
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function statusUpdate(Request $request)
    {
        $merchants = Merchant::whereIn('id', $request->input('merchants', []))->get();
        $checked = $request->has('checked') ? $request->checked : [];

        if ($merchants->isEmpty()) {
            return response()->json(['status' => 'danger', 'message' => __('Nothing has been updated!')]);
        }

        foreach($merchants as $merchant) {
            MerchantMeta::where('key', $request->statusToUpdate)->where('merchant_id', $merchant->id)->delete();

            if (in_array($merchant->id, $checked)) {
                $merchant->meta()->save(new MerchantMeta(['name' => ucfirst($request->statusToUpdate), 'key' => $request->statusToUpdate, 'value' => 'yes']));
            }
        }
           
        return response()->json(['status' => 'success', 'message' => __('Merchants has been updated.')]);
    }

    /**
     * Displays datatables front end view
     *
     * @return \Illuminate\View\View
     */
    public function list()
    {
        $this->authorize('hasAccess', [Merchant::class, 'view']);

        return view('merchants.datatables.index');
    }


    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getList()
    {
        $merchants = Merchant::all();

        return Datatables::of($merchants)
         ->editColumn('category', function(Merchant $merchant){
            return $merchant->category->name;
         })
         ->addColumn('priority', function(Merchant $merchant){
            $status = '<input type="checkbox" name="priority[]" value="' . $merchant->id . '"';

            if ('yes' == $merchant->getMetaByKey('priority')) {
                $status .= ' checked ';
            }

            $status .= '/>';

            return $status;
         })
         ->editColumn('created_at', function(Merchant $merchant){
            return $merchant->created_at->format('D, M j, Y g:i A');
         })
         ->addColumn('action', function(Merchant $merchant){
            $action = '';

            if(Auth::user()->can('hasAccess', [Merchant::class, 'edit'])){
                $action .= '<a href="' . route('merchants.edit', ['merchant' => $merchant->id]) . '"><i class="fa fa-pencil"></i></a> ';
            }

            if(Auth::user()->can('hasAccess', [Merchant::class, 'delete'])){
                $action .= '<a href="#" data-toggle="modal" data-target="#deleteMerchantModal"  data-merchant="' . $merchant->id . '"><i class="fa fa-trash"></i></a> ';
            }

            return $action;

         })
         ->rawColumns(['priority' ,'action'])
         ->toJson();
    }
}

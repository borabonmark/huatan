<?php

namespace App\Http\Controllers;

use App\Tag;
use Illuminate\Http\Request;

class TagController extends Controller
{
    /**
     * Search 
     *
     * @param  \Illuminate\Http\Request  $request
     * @return json
     */
    public function search(Request $request)
    {
        if (!$request->has('term')) {
        	return response()->json([]);
        }  

        $tags = Tag::where(function ($query) use ($request) {
        				$query->where('name', 'LIKE','%'. $request->term .'%');
        			})->get()->pluck('name')->toArray();

        return response()->json($tags);
    }
}

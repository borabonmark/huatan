<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Advertisement;
use App\Http\Resources\Advertisement\Advertisement as AdvertisementResource;

class AdvertisementController extends Controller
{

    public function index()
    {
        return AdvertisementResource::collection(Advertisement::latest()->paginate(1));
    }


    public function create()
    {
        //
    }


    public function show($id)
    {
        //
    }

    public function status($status)
    {
        return AdvertisementResource::collection(Advertisement::where('status', $status)->latest()->get());
    }




}

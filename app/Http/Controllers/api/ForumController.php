<?php

namespace App\Http\Controllers\api;

use Carbon\Carbon;
use App\Post;
use App\PostMeta;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\Post\Post as PostResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class ForumController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return PostResource::collection(Post::latest()->paginate(5));
    }


    public function store(Request $request)
    {
        $post = new Post;
        $post->user_id=Auth::id();       
        $post->content=$request->get('content');
        $post->save();

        if (!empty($request->has('photo'))) {
            Storage::disk('gcs')->put('forums/'. $request->photo->hashName(),file_get_contents($request->photo->getRealPath()));
            $photo = Storage::disk('gcs')->url('forums/'. $request->photo->hashName());

            $post->meta()->save(new PostMeta(['name' => 'Photo', 'key' => 'photo', 'value' =>  $photo]));
        }

        if (!empty($request->has('city'))) {
            $post->meta()->save(new PostMeta(['name' => 'City', 'key' => 'city', 'value' =>  $request->city]));
        }

        if($request->title!=null){
            $post->meta()->save(new PostMeta(['name' => 'Title', 'key' => 'title', 'value' =>  $request->title]));
        }
        
        return  $arr = array('message'=>'posted', 'status'=>'success');
    }


    public function show($city)
    {


        $city = PostMeta::where('value', $city)
                            ->pluck('post_id');

        $post = Post::whereIn('id', $city)->paginate(5);

        return PostResource::collection($post);
    }

    public function byuser($id)
    {
        $post = Post::where('user_id', $id)->paginate(5);
        return PostResource::collection($post);
    }

    public function countbyuser($id)
    {
        return  $arr = array('count'=>Post::where('user_id', $id)->count());
    }

    public function bytoday()
    {
        $post = Post::whereDate('created_at', Carbon::today())->get();        
        return PostResource::collection($post);
    }

    public function countbytoday()
    {        
        return  $arr = array('count'=>Post::whereDate('created_at', Carbon::today())->count());
    }


    public function update(Request $request, $id)
    {
        $post = Post::where('id', $id)
            ->where('user_id', Auth::id())->first();
        
        if($post==null){
            return  $arr = array('status'=>'failed', 'message'=>'this post does not belong to this user');            
        }

        $post->content = $request->get('content');
        $post->save();

        return  $arr = array('status'=>'success', 'message'=>'updated');
    }

    public function destroy($id)
    {

        $post = Post::where('id', $id)
                            ->where('user_id', Auth::id())->first();
        if($post!=null)
        {
            $post->delete();
            return  $arr = array('status'=>'success', 'message'=>'deleted');
        }
        else
        {
            return  $arr = array('status'=>'failed', 'message'=>'not deleted');
        }
        
    }
}

<?php

namespace App\Http\Controllers\api;

use Laravel\Passport\HasApiTokens;

use App\User;
use App\UserMeta;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Hash;
use Semaphore\SemaphoreClient;
use Mail;
use DB;

class RegistrationController extends Controller
{


    public function verify_number(Request $request)
    {

        if (User::where('phone', $request->get('phone'))->exists()) {
            return  $arr = array('status'=>'failed','message' => 'this user already exist');
        }
        else
        {
            $num_str = sprintf("%06d", mt_rand(1, 999999));

            $user = new User();
            $user->phone=$request->get('phone');
            $user->password=Hash::make($num_str);
            $user->role='user';
            $user->save();

            
            $user->meta()->save(new UserMeta(['name' => 'VerificationCode', 'key' => 'verification_code', 'value' =>  $num_str]));

            RegistrationController::sendmessage($request->get('phone'), $num_str);
            return  $arr = array('status'=>'success','message' => $num_str);

        }

    }

    public function reset_password($phone)
    {

        $user = User::where('phone', $phone)->first(); 
        
        if ($user!=null) {
            
            DB::table('oauth_access_tokens')
            ->where('user_id', $user->id)->delete();

            $num_str = sprintf("%06d", mt_rand(1, 999999));

            $user->password=Hash::make($num_str);
            $user->save();
            

            RegistrationController::sendmessage($user->phone, $num_str);    
            return  $arr = array('status'=>'success','message' => $num_str);

        }
        else
        {
            return  $arr = array('status'=>'failed','message' => 'this user not registered');
        }

    }

    public function change_password(Request $request)
    {        
        $id = Auth::id();
        $user = User::find($id);

        DB::table('oauth_access_tokens')
            ->where('user_id', $user->id)->delete();

        $user->password=Hash::make($request->get('password'));
        $user->save();

        return  $arr = array('status'=>'success'); 
                     
    }

    // public function smsverify($code)
    // {        
    //     $id = Auth::id();
    //     $user = User::find($id);        
         
    //     if($user->getMetaByKey('verification_code')==$code)
    //     {
    //         $user->meta()->save(new UserMeta(['name' => 'Verification', 'key' => 'verification', 'value' =>  'verified']));
    //         UserMeta::where('key', 'verification_code')->where('user_id', $id)->delete();
    //         return  $arr = array('status'=>'success');

    //     }
    //     else
    //     {
    //         return  $arr = array('message'=>'failed');   
    //     }                
                     
    // }

    public function resendsms()
    {        
        $id = Auth::id();
        $user = User::find($id);
        $num_str = sprintf("%06d", mt_rand(1, 999999));

        UserMeta::where('key', 'verification_code')->where('user_id', $id)->delete();        
        $user->meta()->save(new UserMeta(['name' => 'VerificationCode', 'key' => 'verification_code', 'value' =>  $num_str])); 
        
        // RegistrationController::sendmessage($user->phone, $num_str);
        return  $arr = array('status'=>'success','message' => $num_str); 
                     
    }

      
    public function sendmessage($phone, $message){

        $ch = curl_init();
        $parameters = array(
            'apikey' => 'e2afd2944b689dfb06ccb1336dbaab68', //Your API KEY
            'number' => $phone,
            'message' => 'Welcome to Huatan!. Your password:'.$message.' From:TSSI team',
            'sendername' => 'Huatan'
        );
        curl_setopt( $ch, CURLOPT_URL,'http://api.semaphore.co/api/v4/messages' );
        curl_setopt( $ch, CURLOPT_POST, 1 );

        //Send the parameters set above with the request
        curl_setopt( $ch, CURLOPT_POSTFIELDS, http_build_query( $parameters ) );

        // Receive response from server
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        $output = curl_exec( $ch );
        curl_close ($ch);

    }

    public function sendemail($code)
    {

        $data = [
        'code' => $code
        ];

        Mail::send('mail.mail', ['data'=>$data], function($message){
            $message->to('taguibaojester@gmail.com', 'Huatan[no-reply]')
                    ->from('no-reply@huatan.com.ph', 'Huatan Team')
                    ->subject('Welcome to huatan');
        });

    }
}
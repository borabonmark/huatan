<?php

namespace App\Http\Controllers\api;

use App\User;
use App\UserMeta;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\Http\Resources\User\User as UserResource;

class UserDetailController extends Controller
{

    public function index()
    {
        $id = Auth::id();

        return UserResource::collection(User::where('id', $id)->get()); 
    }


    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        $id = Auth::id();
        $user = User::find($id);

        UserMeta::where('key', 'firstname')->where('user_id', $id)->delete();
        if (!empty($request->has('firstname'))) {
            $user->meta()->save(new UserMeta(['name' => 'FirstName', 'key' => 'firstname', 'value' =>  $request->firstname]));
        }

        UserMeta::where('key', 'lastname')->where('user_id', $id)->delete();
        if (!empty($request->has('lastname'))) {
            $user->meta()->save(new UserMeta(['name' => 'LastName', 'key' => 'lastname', 'value' =>  $request->lastname]));
        }

        UserMeta::where('key', 'city')->where('user_id', $id)->delete();
        if (!empty($request->has('city'))) {
            $user->meta()->save(new UserMeta(['name' => 'City', 'key' => 'city', 'value' =>  $request->city]));
        }

        UserMeta::where('key', 'age')->where('user_id', $id)->delete();
        if (!empty($request->has('age'))) {
            $user->meta()->save(new UserMeta(['name' => 'Age', 'key' => 'age', 'value' =>  $request->age]));
        }

        UserMeta::where('key', 'height')->where('user_id', $id)->delete();
        if (!empty($request->has('height'))) {
            $user->meta()->save(new UserMeta(['name' => 'Height', 'key' => 'height', 'value' =>  $request->height]));
        }

        UserMeta::where('key', 'weight')->where('user_id', $id)->delete();
        if (!empty($request->has('weight'))) {
            $user->meta()->save(new UserMeta(['name' => 'Weight', 'key' => 'weight', 'value' =>  $request->weight]));
        }

        UserMeta::where('key', 'status')->where('user_id', $id)->delete();
        if (!empty($request->has('status'))) {
            $user->meta()->save(new UserMeta(['name' => 'Status', 'key' => 'status', 'value' =>  $request->status]));
        }

        UserMeta::where('key', 'gender')->where('user_id', $id)->delete();
        if (!empty($request->has('gender'))) {
            $user->meta()->save(new UserMeta(['name' => 'Gender', 'key' => 'gender', 'value' =>  $request->gender]));
        }

        UserMeta::where('key', 'description')->where('user_id', $id)->delete();
        if (!empty($request->has('description'))) {
            $user->meta()->save(new UserMeta(['name' => 'Description', 'key' => 'description', 'value' =>  $request->description]));
        }

        UserMeta::where('key', 'chat_id')->where('user_id', $id)->delete();
        if (!empty($request->has('chat_id'))) {
            $user->meta()->save(new UserMeta(['name' => 'ChatID', 'key' => 'chat_id', 'value' =>  $request->chat_id]));
        }

        return  $arr = array('message'=>'success');
    }

    public function storeavatar(Request $request)
    {
        $id = Auth::id();
        $user = User::find($id);
    

        UserMeta::where('key', 'avatar')->where('user_id', $id)->delete();
        if (!empty($request->has('avatar'))) {
            Storage::disk('gcs')->put('avatar/'. $request->avatar->hashName(),file_get_contents($request->avatar->getRealPath()));
            $avatar = Storage::disk('gcs')->url('avatar/'. $request->avatar->hashName());
            $user->meta()->save(new UserMeta(['name' => 'Avatar', 'key' => 'avatar', 'value' =>  $avatar]));
        }

        return  $arr = array('message'=>'success');
    }

    public function geolocation(Request $request)
    {
        $id = Auth::id();
        $user = User::find($id);
    
        $longitude = UserMeta::where('key', 'longitude')->where('user_id', $id)->first();
        $latitude = UserMeta::where('key', 'latitude')->where('user_id', $id)->first();

        if($longitude!=null && $latitude!=null)
        {
            $longitude->value=$request->longitude;
            $longitude->save();
            $latitude->value=$request->latitude;
            $latitude->save();
            return  $arr = array('message'=>'updated', 'status'=>'success');
        }
        else
        {
            if(!empty($request->has('longitude'))) {
                $user->meta()->save(new UserMeta(['name' => 'Longitude', 'key' => 'longitude', 'value' =>  $request->longitude]));
            }

            UserMeta::where('key', 'latitude')->where('user_id', $id)->delete();
            if (!empty($request->has('latitude'))) {
                $user->meta()->save(new UserMeta(['name' => 'Latitude', 'key' => 'latitude', 'value' =>  $request->latitude]));
            }        

            return  $arr = array('message'=>'created', 'status'=>'success');
        }

    }


    public function online()
    {
        $id = Auth::id();
        $user = User::find($id);
    
        $online = UserMeta::where('key', 'online')->where('user_id', $id)->first();

        if($online!=null)
        {
            if($online->value==true)
            {
                $online->value = false;
                $online->save();                
                return  $arr = array('message'=>'updated', 'status'=>'offline');
            }

            $online->value = true;
            $online->save();

            return  $arr = array('message'=>'updated', 'status'=>'online');
        }
        else
        {
            $user->meta()->save(new UserMeta(['name' => 'Online', 'key' => 'online', 'value' =>  true]));
            return  $arr = array('message'=>'updated', 'status'=>'success');
        }
    }

    public function show($id)
    {
        return UserResource::collection(User::where('id', $id)
                                            ->where('role', 'user')->get());
    }


    public function update(Request $request)
    {

        $id = Auth::id();
        UserMeta::where('key', 'priority')->where('user_id', $id)->delete();
        return 'success';
    }


    public function destroy($id)
    {
        //
    }
}

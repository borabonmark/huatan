<?php

namespace App\Http\Controllers\api;

use App\User;
use App\UserMeta;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class UserImageController extends Controller
{

    public function index()
    {
        //
    }


    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        $id = Auth::id();
        $user = User::find($id);

        $imagecount = UserMeta::where('key', 'photo')->where('user_id', $id)->count();

        if($imagecount!=9)
        {
            if (!empty($request->has('photo'))) {
                Storage::disk('gcs')->put('userimages/'. $request->photo->hashName(),file_get_contents($request->photo->getRealPath()));
                $photo = Storage::disk('gcs')->url('userimages/'. $request->photo->hashName());

                $user->meta()->save(new UserMeta(['name' => 'Photo', 'key' => 'photo', 'value' =>  $photo]));

                return  $arr = array('message'=>'added new photo', 'status'=>'success');
            }

        }
        else
        {
            return  $arr = array('message'=>'max image only 9', 'status'=>'failed');
        }
        
    }


    public function show($id)
    {
        
    }


    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        UserMeta::where('key', 'photo')->where('id', $id)->where('user_id', Auth::id())->delete();
        return  $arr = array('message'=>'deleted', 'status'=>'success');
    }
}

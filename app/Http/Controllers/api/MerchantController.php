<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Merchant;
use App\Http\Resources\Merchant\Merchant as MerchantResource;

class MerchantController extends Controller
{

    public function index()
    {
        return MerchantResource::collection(Merchant::latest()->paginate(5));
    }

    public function priority()
    {

        $merchant=Merchant::whereHas('meta', function($query){
            $query->where('key', 'priority');
            $query->where('value', 'yes');
        })->paginate(5);
        
        return MerchantResource::collection($merchant);
    }


    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        //
    }


    public function show($id)
    {
        //
    }

    function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        //
    }
}

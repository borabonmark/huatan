<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Bookmark;
use App\Merchant;
use App\Article;
use App\User;
use App\Post;
use App\Http\Resources\Merchant\Merchant as MerchantResource;
use App\Http\Resources\Article as ArticleResource;
use App\Http\Resources\User\User as UserResource;
use App\Http\Resources\Post\Post as PostResource;

class BookmarkController extends Controller
{

    public function index()
    {
        //
    }


    public function store(Request $request)
    {

        $bookmarkfind = Bookmark::where('bookmarkable_id', $request->get('bookmarkable_id'))
                            ->where('bookmarkable_type', $request->get('bookmarkable_type'))
                            ->where('user_id', Auth::id())->first();
        
        if($bookmarkfind!=null)
        {
            return  $arr = array('status'=>'failed', 'message'=>'it was already bookmarked');
        }
        else
        {
            $bookmark = new Bookmark;
            $bookmark->user_id=Auth::id();
            $bookmark->bookmarkable_id=$request->get('bookmarkable_id');
            $bookmark->bookmarkable_type=$request->get('bookmarkable_type');
            $bookmark->save();

            return  $arr = array('status'=>'success');

        }

        
    }

    public function check($id, $type)
    {
        $bookmark = Bookmark::where('bookmarkable_id', $id)
                            ->where('bookmarkable_type', 'App\\'.$type)
                            ->where('user_id', Auth::id())->first();

        if($bookmark!=null)
        {
            return  $arr = array('status'=>'success', 'bookmark'=>'true');
        }
        else
        {
            return  $arr = array('status'=>'success', 'bookmark'=>'no');   
        }

        
    }
    
    public function show_merchant()
    {
        $bookmark = Bookmark::where('bookmarkable_type', 'App\Merchant')
                            ->where('user_id', Auth::id())->pluck('bookmarkable_id');

        $merchant = Merchant::whereIn('id', $bookmark)->paginate(5);
        
        return MerchantResource::collection($merchant);
    }

    public function show_article()
    {
        $bookmark = Bookmark::where('bookmarkable_type', 'App\Article')
                            ->where('user_id', Auth::id())->pluck('bookmarkable_id');

        $article = Article::whereIn('id', $bookmark)->paginate(5);
        
        return ArticleResource::collection($article);
    }

    public function show_user()
    {
        $bookmark = Bookmark::where('bookmarkable_type', 'App\User')
                            ->where('user_id', Auth::id())->pluck('bookmarkable_id');

        $user = User::whereIn('id', $bookmark)->paginate(5);
        
        return UserResource::collection($user);
    }

    public function show_post()
    {
        $bookmark = Bookmark::where('bookmarkable_type', 'App\Post')
                            ->where('user_id', Auth::id())->pluck('bookmarkable_id');

        $post = Post::whereIn('id', $bookmark)->paginate(5);
        
        return PostResource::collection($post);
    }

    public function destroy($id, $type)
    {
        $bookmark = Bookmark::where('bookmarkable_id', $id)
                    ->where('bookmarkable_type', 'App\\'.$type)
                    ->where('user_id', Auth::id())->first();
        if($bookmark!=null)
        {
            $bookmark->delete();
            return  $arr = array('status'=>'success', 'message'=>'bookmark removed');

        }
        else
        {
            return  $arr = array('status'=>'failed', 'message'=>'book not existing');
        }
        


    }
}

<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Article;
use App\ArticleMeta;
use App\Http\Resources\Article as ArticleResource;

// use Resources\News\NewsResource;

class ArticleController extends Controller
{

    public function index()
    {
         return ArticleResource::collection(Article::latest()->paginate(5));
    }

    public function paginateby($count)
    {
         return ArticleResource::collection(Article::latest()->paginate($count));
    }

    public function bycategory($category_id)
    {
         return ArticleResource::collection(Article::where('category_id', $category_id)->latest()->paginate(5));
    }


    public function show(Article $article)
    {
        // return Article::find($id);
        return new ArticleResource($article);
    }

    public function click($id)
    {
        $article = Article::find($id);
        $articlemeta = ArticleMeta::where('article_id', $id)->where('key', 'click')->first();

        if($articlemeta==null)
        {
            $article->meta()->save(new ArticleMeta(['name' => 'Click', 'key' => 'click', 'value' =>  1]));
            return  $arr = array('message'=>'success');
        }
        else
        {
            $articlemeta->value = $articlemeta->value+1;
            $articlemeta->save();
            return  $arr = array('message'=>'success');
        }
        
    }

}

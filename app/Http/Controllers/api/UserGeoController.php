<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\UserMeta;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\User\User as UserResource;


class UserGeoController extends Controller
{

    public function index()
    {
        return UserResource::collection(User::where('role', 'user')->get());
    }

}

<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Comment;
use App\CommentMeta;
use App\Http\Resources\Comment\Comment as CommentResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class CommentController extends Controller
{

    public function index()
    {
        return CommentResource::collection(Comment::all());
    }

    public function store(Request $request)
    {
        
        $request->validate([
            'content' => 'required|string|max:255',
        ]);

        $comment = new Comment;
        $comment->user_id=Auth::id();
        $comment->content=$request->get('content');
        $comment->commentable_id=$request->get('commentable_id');
        $comment->commentable_type=$request->get('commentable_type');
        $comment->save();

        return  $arr = array('status'=>'success', 'message'=>'posted');
    }

    public function storereply(Request $request)
    {
        
        $request->validate([
            'content' => 'required|string|max:255',
        ]);

        $comment = new Comment;
        $comment->user_id=Auth::id();
        $comment->parent_id=$request->get('parent_id');
        $comment->content=$request->get('content');
        $comment->commentable_id=$request->get('commentable_id');
        $comment->commentable_type=$request->get('commentable_type');
        $comment->save();

        return  $arr = array('status'=>'success', 'message'=>'reply success');
    }

    public function filestore(Request $request)
    {  

        $comment = new Comment;
        $comment->user_id=Auth::id();       
        $comment->commentable_id=$request->get('commentable_id');
        $comment->commentable_type=$request->get('commentable_type');

        Storage::disk('gcs')->put('comments/'. $request->content->hashName(),file_get_contents($request->content->getRealPath()));
        $content = Storage::disk('gcs')->url('comments/'. $request->content->hashName());
        $comment->content = $content;

        $comment->save();

        CommentMeta::where('key', 'description')->where('comment_id', $comment->id)->delete();
        if (!empty($request->has('description'))) {
            $comment->meta()->save(new CommentMeta(['name' => 'Description', 'key' => 'description', 'value' => $request->description]));
        }

        return  $arr = array('status'=>'success', 'message'=>'file stored');
    }

    public function filestorereply(Request $request)
    {  

        $comment = new Comment;
        $comment->user_id=Auth::id();       
        $comment->commentable_id=$request->get('commentable_id');
        $comment->commentable_type=$request->get('commentable_type');
        $comment->parent_id=$request->get('parent_id');

        Storage::disk('gcs')->put('comments/'. $request->content->hashName(),file_get_contents($request->content->getRealPath()));
        $content = Storage::disk('gcs')->url('comments/'. $request->content->hashName());
        $comment->content = $content;

        $comment->save();

        CommentMeta::where('key', 'description')->where('comment_id', $comment->id)->delete();
        if (!empty($request->has('description'))) {
            $comment->meta()->save(new CommentMeta(['name' => 'Description', 'key' => 'description', 'value' => $request->description]));
        }

        return  $arr = array('status'=>'success', 'message'=>'reply file stored');
    }


    public function show($id, $type)
    {

        return CommentResource::collection(Comment::where('commentable_id', $id)
                                            ->where('commentable_type', 'App\\'.$type)
                                            ->where('parent_id', null)
                                            ->oldest()->get());   
    }

    public function reply($id, $comment, $type)
    {

        return CommentResource::collection(Comment::where('commentable_id', $id)
                                            ->where('parent_id', $comment)
                                            ->where('commentable_type', 'App\\'.$type)
                                            ->oldest()->get());   
    }


    public function update(Request $request, $id)
    {
        $comment = Comment::where('id', $id)
                    ->where('user_id', Auth::id())->first();

        $comment->content = $request->get('content');
        $comment->save();

        return  $arr = array('status'=>'success', 'message'=>'updated');
    }


    public function destroy($id)
    {
        $comment = Comment::where('id', $id)
                            ->where('user_id', Auth::id())->first();
        $comment->delete();

        return  $arr = array('status'=>'success', 'message'=>'deleted');
    }
}

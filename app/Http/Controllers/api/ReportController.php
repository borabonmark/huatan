<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Report;

class ReportController extends Controller
{

    public function index()
    {
        //
    }


    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        $report = new Report;
        $report->user_id=Auth::id();       
        $report->content=$request->get('content');
        $report->reportable_id=$request->get('reportable_id');
        $report->reportable_type=$request->get('reportable_type');
        $report->save();

        return array('status'=>'success', 'message'=>'posted');
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}

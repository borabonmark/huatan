<?php

namespace App\Http\Controllers;

use App\User;
use App\Module;
use DataTables;
use App\UserMeta;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    /**
     * Pagination max no. of items per page
     */
    public $itemsPerPage = 20;

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('hasAccess', [User::class, 'view']);

        $users = User::orderBy('id', 'desc')
                             ->where(function ($query) use ($request) {
                                $query->where('role', '<>', User::ROLE_SUPER_ADMIN);

                                // Search by firstname or lastname
                                if (!empty($request->name)) {
                                    $query->whereHas('meta', function($query) use ($request) {
                                        $query->whereIn('key', ['firstname', 'lastname']);
                                        $query->where('value', 'LIKE','%'. $request->name .'%');
                                    }); 
                                }
                             })->paginate($this->itemsPerPage);

        // To add search on pagination
        $users->appends($request->all());

        // To repopulate search input
        $request->flash();

        return view('users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('hasAccess', [User::class, 'add']);
        $statuses = User::$statuses;

        return view('users.create', compact('modules', 'statuses'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Validator::make($request->all(), [
            'firstname' => 'required|string|max:255',
            'lastname' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'phone' => 'required|string|unique:users',
            'status' => ['required', Rule::in(User::$statuses)],
            'password' => 'required|string|min:6|confirmed'
        ])->validate();

        $user = User::create([
            'email' => $request->email,
            'phone' => $request->phone,
            'role' => User::ROLE_ADMIN,
            'status' => $request->status,
            'password' => Hash::make($request->password)
        ]);

        $user->meta()->createMany([
            ['name' => 'FirstName', 'key' => 'firstname', 'value' =>  $request->firstname],
            ['name' => 'LastName', 'key' => 'lastname', 'value' =>  $request->lastname]
        ]);

        if (!Auth::user()->can('hasAccess', [User::class, 'edit'])) {
            return back()->with('status', 'success')
                         ->with('message', '<i class="fa fa-check-circle"></i> ' . __('User has been saved.'));
        }

        return redirect()->route('users.edit', ['user' => $user->id])
                         ->with('status_for', 'user_info')
                         ->with('status', 'success')
                         ->with('message', '<i class="fa fa-check-circle"></i> ' . __('User has been saved.'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $this->authorize('hasAccess', [User::class, 'edit']);

        $statuses = User::$statuses;
        $modules = Module::all();
        $actions = Module::$actions;

        return view('users.edit', compact('user', 'statuses', 'modules', 'actions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        Validator::make($request->all(), [
            'firstname' => 'required|string|max:255',
            'lastname' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users,email,' . $user->id,
            'phone' => 'required|string|unique:users,phone,' . $user->id,
            'status' => ['required', Rule::in(User::$statuses)],
        ])->validate();

        $user->fill($request->only(['email', 'phone', 'status']))->save();

        UserMeta::whereIn('key', ['firstname', 'lastname'])->where('user_id', $user->id)->delete();
        $user->meta()->createMany([
            ['name' => 'FirstName', 'key' => 'firstname', 'value' =>  $request->firstname],
            ['name' => 'LastName', 'key' => 'lastname', 'value' =>  $request->lastname]
        ]);

        return back()->with('status_for', 'user_info')
                     ->with('status', 'success')
                     ->with('message', '<i class="fa fa-check-circle"></i> ' . __('User has been updated.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $this->authorize('hasAccess', [User::class, 'delete']);
        $user->delete();
        return back()->with('status', 'success')
                     ->with('message', '<i class="fa fa-check-circle"></i> ' . __('User has been deleted.'));
    }

    /**
     * Change Password
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function changePassword(Request $request, User $user)
    {
        Validator::make($request->all(), [
            'password' => 'required|string|min:6|confirmed'
        ], [
            'password.required' => 'Password is required.'
        ])->validate();

        $user->password = Hash::make($request->password);
        $user->save();

        return back()->with('status_for', 'change_password')
                     ->with('status', 'success')
                     ->with('message', '<i class="fa fa-check-circle"></i> '. __('Password has been changed.'));
    }

    /**
     * Assign modules access.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function assignModules(Request $request, User $user)
    {
        if (!$request->has('modules_access')) {
            return back()->with('status_for', 'modules_access')
                         ->with('status', 'danger')
                         ->with('message', '<i class="fa fa-times-circle"></i> ' . __('Please select modules access.'));
        }

        $user->modules()->detach();

        foreach($request->modules_access as $module => $access) {
            foreach($access as $action) {
                $user->modules()->attach($module, ['action' => $action]);
            }
        }

        return back()->with('status_for', 'modules_access')
                         ->with('status', 'success')
                         ->with('message', '<i class="fa fa-check-circle"></i> ' . __('Modules access has been saved.'));
    }

    /**
     * Update status
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function statusUpdate(Request $request)
    {
        $users = User::whereIn('id', $request->input('users', []))->get();
        $checked = $request->has('checked') ? $request->checked : [];

        if ($users->isEmpty()) {
            return response()->json(['status' => 'danger', 'message' => __('Nothing has been updated!')]);
        }

        foreach($users as $user) {
            $user->status = in_array($user->id, $checked) ? User::STATUS_ACTIVE : User::STATUS_INACTIVE;
            $user->save();          
        }
           
        return response()->json(['status' => 'success', 'message' => __('Users has been updated')]);
    }

    /**
     * Displays datatables front end view
     *
     * @return \Illuminate\View\View
     */
    public function list($role = 'user')
    {
        $this->authorize('hasAccess', [User::class, 'view']);

        return view('users.datatables.index', compact('role'));
    }


    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getList($role = 'user')
    {
        $users = User::where('role', '=', $role)->get();

        return Datatables::of($users)
         ->addColumn('fullname', function(User $user){
            return $user->firstname . ' ' . $user->lastname;
         })
         ->editColumn('role', function(User $user){
            return ucfirst($user->role);
         })
         ->editColumn('status', function(User $user){
            $status = '<input type="checkbox" name="status[]" value="' . $user->id . '"';

            if (User::STATUS_ACTIVE == $user->status) {
                $status .= ' checked ';
            }

            $status .= '/>';

            return $status;
         })
         ->addColumn('action', function(User $user){
            $action = '';

            if(Auth::user()->can('hasAccess', [User::class, 'edit'])){
                $action .= '<a href="' . route('users.edit', ['user' => $user->id]) . '"><i class="fa fa-pencil"></i></a> ';
            }

            if(Auth::user()->can('hasAccess', [User::class, 'delete'])){
                $action .= '<a href="#" data-toggle="modal" data-target="#deleteUserModal"  data-user="' . $user->id . '"><i class="fa fa-trash"></i></a> ';
            }

            return $action;

         })
         ->rawColumns(['action', 'status'])
         ->toJson();
    }
}

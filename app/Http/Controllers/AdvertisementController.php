<?php

namespace App\Http\Controllers;

use DataTables;
use Carbon\Carbon;
use App\Advertisement;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Traits\CustomImageUpload;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AdvertisementController extends Controller
{
    use CustomImageUpload;

    /**
     * Pagination max no. of items per page
     */
    public $itemsPerPage = 20;

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('hasAccess', [Advertisement::class, 'view']);

        $advertisements = Advertisement::orderBy('id', 'desc')
                                       ->where(function ($query) use ($request) {
                                            // Search by name
                                            if (!empty($request->name)) {
                                                $query->where('name', 'LIKE','%'. $request->name .'%');
                                            }
                                        })->paginate($this->itemsPerPage);

        // To add search on pagination
        $advertisements->appends($request->all());

        // To repopulate search input
        $request->flash();

        return view('advertisements.index', compact('advertisements'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('hasAccess', [Advertisement::class, 'add']);
        $material = Advertisement::$material; 

        return view('advertisements.create', compact('material'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->only([
            'name', 'title', 'url', 'date_time_online', 'status', 'material'
        ]);

        $this->validator($data)->validate();

        $data['material'] = $this->customImageUpload($request->material, 'images', 'gcs', 
                        Advertisement::$material['width'], Advertisement::$material['height']);

        $data['date_time_online'] = Carbon::parse($data['date_time_online']);

        $advertisement = Advertisement::create($data);

        if (!Auth::user()->can('hasAccess', [Advertisement::class, 'edit'])) {
            return back()->with('status', 'success')
                         ->with('message', '<i class="fa fa-check-circle"></i> ' . __('Advertisement has been saved.'));
        }

        return redirect()->route('advertisements.edit', ['advertisement' => $advertisement->id])
                         ->with('status', 'success')
                         ->with('message', '<i class="fa fa-check-circle"></i> ' . __('Advertisement has been saved.'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Advertisement  $advertisement
     * @return \Illuminate\Http\Response
     */
    public function show(Advertisement $advertisement)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Advertisement  $advertisement
     * @return \Illuminate\Http\Response
     */
    public function edit(Advertisement $advertisement)
    {
        $this->authorize('hasAccess', [Advertisement::class, 'edit']);
        $material = Advertisement::$material;

        return view('advertisements.edit', compact('advertisement', 'material'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Advertisement  $advertisement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Advertisement $advertisement)
    {
        $data = $request->only([
            'name', 'title', 'url', 'date_time_online', 'status', 'material'
        ]);

        $this->validator($data, $advertisement)->validate();

        if ($request->has('material')) {
            $data['material'] = $this->customImageUpload($request->material, 'images', 'gcs', 
                        Advertisement::$material['width'], Advertisement::$material['height']);
        }

        $data['date_time_online'] = Carbon::parse($data['date_time_online'])->toDateTimeString();

        $advertisement->fill($data)->save();

        return back()->with('status', 'success')
                     ->with('message', '<i class="fa fa-check-circle"></i> ' . __('Advertisement has been updated.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Advertisement  $advertisement
     * @return \Illuminate\Http\Response
     */
    public function destroy(Advertisement $advertisement)
    {
        $this->authorize('hasAccess', [Advertisement::class, 'delete']);
        $advertisement->delete();
        return back()->with('status', 'success')
                     ->with('message', '<i class="fa fa-check-circle"></i> ' . __('Advertisement has been deleted.'));
    }

    /**
     * Create validator
     *
     * @param  array  $data
     * @param  \App\Advertisement  $advertisement
     * @return \Illuminate\Http\Response
     */
    protected function validator(array $data, $advertisement = NULL)
    {
        $width = Advertisement::$material['width'];
        $height = Advertisement::$material['height'];
        $size = Advertisement::$material['size'] * 1024;

        $rules = [
            'name' => 'required|string|max:255',
            'title' => 'required|string|max:255',
            'url' => 'required|url',
            'date_time_online' => 'required|date',
            'status' => ['required', Rule::in(Advertisement::$statuses)],
            'material' => [
                'required', 
                'image',
                'max:' . $size,
                Rule::dimensions()->minWidth($width)
                                  ->minHeight($height)
            ]
        ];

        if ($advertisement) {
            unset($rules['material']['required']);
            array_unshift($rules['material'], 'sometimes');
        }

        return Validator::make($data, $rules);
    }

    /**
     * Update status
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function statusUpdate(Request $request)
    {
        $advertisements = Advertisement::whereIn('id', $request->input('advertisements', []))->get();
        $checked = $request->has('checked') ? $request->checked : [];

        if ($advertisements->isEmpty()) {
            return response()->json(['status' => 'danger', 'message' => __('Nothing has been updated!')]);
        }

        foreach($advertisements as $advertisement) {
            $advertisement->status = in_array($advertisement->id, $checked) ? 
                                     Advertisement::STATUS_ONLINE : Advertisement::STATUS_OFFLINE;
            $advertisement->save();          
        }
           
        return response()->json(['status' => 'success', 'message' => __('Advertisements has been updated')]);
    }

    /**
     * Displays datatables front end view
     *
     * @return \Illuminate\View\View
     */
    public function list()
    {
        $this->authorize('hasAccess', [Advertisement::class, 'view']);

        return view('advertisements.datatables.index');
    }


    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getList()
    {
        $advertisements = Advertisement::all();

        return Datatables::of($advertisements)
         ->editColumn('status', function(Advertisement $advertisement){
            $status = '<input type="checkbox" name="status[]" value="' . $advertisement->id . '"';

            if ('online' == $advertisement->status) {
                $status .= ' checked ';
            }

            $status .= '/>';

            return $status;
         })
         ->editColumn('date_time_online', function(Advertisement $advertisement){
            return $advertisement->date_time_online->format('D, M j, Y g:i A');
         })
         ->editColumn('created_at', function(Advertisement $advertisement){
            return $advertisement->created_at->format('D, M j, Y g:i A');
         })
         ->addColumn('action', function(Advertisement $advertisement){
            $action = '';

            if(Auth::user()->can('hasAccess', [Advertisement::class, 'edit'])){
                $action .= '<a href="' . route('advertisements.edit', ['advertisement' => $advertisement->id]) . '"><i class="fa fa-pencil"></i></a> ';
            }

            if(Auth::user()->can('hasAccess', [Advertisement::class, 'delete'])){
                $action .= '<a href="#" data-toggle="modal" data-target="#deleteAdvertisementsModal"  data-advertisement="' . $advertisement->id . '"><i class="fa fa-trash"></i></a> ';
            }

            return $action;

         })
         ->rawColumns(['status', 'action'])
         ->toJson();
    }
}

<?php

namespace App\Http\Controllers;

use App\Tag;
use App\User;
use DataTables;
use App\Article;
use App\Taggable;
use App\Category;
use Carbon\Carbon;
use App\ArticleMeta;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Traits\CustomImageUpload;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ArticleController extends Controller
{
    use CustomImageUpload;

    /**
     * Pagination max no. of items per page
     */
    public $itemsPerPage = 20;

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('hasAccess', [Article::class, 'view']);

        $articles = Article::orderBy('id', 'desc')
                             ->where(function ($query) use ($request) {
                                // Search by title
                                if (!empty($request->title)) {
                                    $query->where('title', 'LIKE','%'. $request->title .'%');
                                }

                                // Show only the articles belongs to user if not super admin
                                if (!Auth::user()->hasRole(User::ROLE_SUPER_ADMIN)) {
                                    $query->where('user_id', Auth::id());
                                }
                             })->paginate($this->itemsPerPage);

        // To add search on pagination
        $articles->appends($request->all());

        // To repopulate search input
        $request->flash();

        return view('articles.index', compact('articles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    { 
        $this->authorize('hasAccess', [Article::class, 'add']);

        $categories = Category::where('type', Category::TYPE_NEWS)->get();
        $photo = Article::$photo;

        return view('articles.create', compact('categories', 'photo'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('hasAccess', [Article::class, 'add']);

        $this->validator($request->all())->validate();

        $article = Article::create([
            'user_id' => Auth::id(),
            'category_id' => $request->category_id,
            'title' => $request->title,
            'summary' => $request->summary,
            'content' => $request->content,
            'publish' => $request->publish,
            'date_time_published' => Carbon::parse($request->date_time_published)
        ]);

        if (!empty($request->has('recommended'))) {
            $article->meta()->save(new ArticleMeta(['name' => 'Recommended', 'key' => 'recommended', 'value' => 'yes']));
        }

        if (!empty($request->has('photo'))) {
            foreach($request->photo as $photo) {
                $article->meta()->save(new ArticleMeta(['name' => 'Photo', 'key' => 'photo', 'value' => $photo]));
            }
        }

        if (!empty($request->has('thumbnail'))) {
            foreach($request->thumbnail as $thumbnail) {
                $article->meta()->save(new ArticleMeta(['name' => 'Thumbnail', 'key' => 'thumbnail', 'value' => $thumbnail]));
            }
        }

        if (!empty($request->has('tag'))) {
            foreach($request->tag as $tag) {
                $tag = Tag::firstOrCreate(['name' => $tag]);

                Taggable::create([
                    'tag_id' => $tag->id,
                    'taggable_id' => $article->id,
                    'taggable_type' => Article::class
                ]);
            }
        }

        if (!Auth::user()->can('hasAccess', [Article::class, 'edit'])) {
            return back()->with('status', 'success')
                         ->with('message', '<i class="fa fa-check-circle"></i> ' . __('Article has been saved.'));
        }

        return redirect()->route('articles.edit', ['article' => $article->id])
                         ->with('status', 'success')
                         ->with('message', '<i class="fa fa-check-circle"></i> ' . __('Article has been saved.'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function show(Article $article)
    {
        return view('articles.show', compact('article'));
    }

    public function webView(Article $article)
    {
        return view('articles.show', compact('article'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function edit(Article $article)
    {
        $this->authorize('hasAccess', [Article::class, 'edit']);
        $this->authorize('belongsTo', $article);

        $categories = Category::where('type', Category::TYPE_NEWS)->get();
        $photo = Article::$photo;

        return view('articles.edit', compact('article', 'categories', 'photo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Article $article)
    {
        $this->authorize('hasAccess', [Article::class, 'edit']);
        $this->authorize('belongsTo', $article);

        $this->validator($request->all())->validate();

        $article->category_id = $request->category_id;
        $article->title = $request->title;
        $article->summary = $request->summary;
        $article->content = $request->content;
        $article->publish = $request->publish;
        $article->date_time_published = Carbon::parse($request->date_time_published);
        $article->save();

        ArticleMeta::where('key', 'recommended')->where('article_id', $article->id)->delete();
        if (!empty($request->has('recommended'))) {
            $article->meta()->save(new ArticleMeta(['name' => 'Recommended', 'key' => 'recommended', 'value' => 'yes']));
        }

        if (!empty($request->has('photo'))) {
            ArticleMeta::where('key', 'photo')->where('article_id', $article->id)->delete();

            foreach($request->photo as $photo) {
                $article->meta()->save(new ArticleMeta(['name' => 'Photo', 'key' => 'photo', 'value' => $photo]));
            }
        }

        if (!empty($request->has('thumbnail'))) {
            ArticleMeta::where('key', 'thumbnail')->where('article_id', $article->id)->delete();

            foreach($request->thumbnail as $thumbnail) {
                $article->meta()->save(new ArticleMeta(['name' => 'Thumbnail', 'key' => 'thumbnail', 'value' => $thumbnail]));
            }
        }

        if (!empty($request->has('tag'))) {
            Taggable::where('taggable_id', $article->id)
                    ->where('taggable_type', Article::class)
                    ->delete();

            foreach($request->tag as $tag) {
                $tag = Tag::firstOrCreate(['name' => $tag]);

                Taggable::create([
                    'tag_id' => $tag->id,
                    'taggable_id' => $article->id,
                    'taggable_type' => Article::class
                ]);
            }
        }

        return back()->with('status', 'success')
                     ->with('message', '<i class="fa fa-check-circle"></i> ' . __('Article has been updated.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function destroy(Article $article)
    {
        $this->authorize('hasAccess', [Article::class, 'delete']);
        $this->authorize('belongsTo', $article);

        $article->delete();

        return back()->with('status', 'success')
                     ->with('message', '<i class="fa fa-check-circle"></i> ' . __('Article has been deleted.'));
    }

    /**
     * Create validator
     *
     * @param  array  $data
     * @return \Illuminate\Http\Response
     */
    protected function validator(array $data)
    {
        $rules = [
            'category_id' => 'required|exists:categories,id',
            'title' => 'required|string|max:255',
            'summary' => 'required|string',
            'content' => 'required|string',
            'publish' => ['required', Rule::in(Article::$publishStatuses)],
            'date_time_published' => 'required|date'
        ];

        return Validator::make($data, $rules);
    }

    /**
     * Update article publish or recommended status 
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function statusUpdate(Request $request)
    {
        $articles = Article::whereIn('id', $request->input('articles', []))->get();
        $checked = $request->has('checked') ? $request->checked : [];

        if ($articles->isEmpty()) {
            return response()->json(['status' => 'danger', 'message' => __('Nothing has been updated!')]);
        }

        foreach($articles as $article) {
            if ('publish' == $request->statusToUpdate) {
                $article->publish = in_array($article->id, $checked) ? 'yes' : 'no';
                $article->save();
                continue;
            }

            ArticleMeta::where('key', $request->statusToUpdate)->where('article_id', $article->id)->delete();

            if (in_array($article->id, $checked)) {
                $article->meta()->save(new ArticleMeta(['name' => ucfirst($request->statusToUpdate), 'key' => $request->statusToUpdate, 'value' => 'yes']));
            }            
        }
           
        return response()->json(['status' => 'success', 'message' => __('Articles has been updated.')]);
    }

    /**
     * Displays datatables front end view
     *
     * @return \Illuminate\View\View
     */
    public function list()
    {
        $this->authorize('hasAccess', [Article::class, 'view']);

        return view('articles.datatables.index');
    }

    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getList()
    {
        $articles = Article::all();

        return Datatables::of($articles)
         ->addColumn('category', function(Article $article){
            return $article->category->name;
         })
         ->addColumn('author', function(Article $article){
            return $article->user->firstname . ' ' . $article->user->firstname;
         })
         ->editColumn('recommended', function(Article $article){
            $recommended = '<input type="checkbox" name="recommended[]" value="' . $article->id . '"';

            if ('yes' == $article->getMetaByKey('recommended')) {
                $recommended .= ' checked ';
            }

            $recommended .= '/>';

            return $recommended;
         })
         ->editColumn('publish', function(Article $article){
            $publish = '<input type="checkbox" name="publish[]" value="' . $article->id . '"';

            if ('yes' == $article->publish) {
                $publish .= ' checked ';
            }

            $publish .= '/>';

            return $publish;
         })
         ->addColumn('date_time_published', function(Article $article){
            return $article->date_time_published->format('D, M j, Y g:i A');
         })
         ->addColumn('action', function(Article $article){
            $action = '';

            if(Auth::user()->can('hasAccess', [Article::class, 'edit'])){
                $action .= '<a href="' . route('articles.edit', ['article' => $article->id]) . '"><i class="fa fa-pencil"></i></a> ';
            }

            if(Auth::user()->can('hasAccess', [Article::class, 'delete'])){
                $action .= '<a href="#" data-toggle="modal" data-target="#deleteArticleModal"  data-article="' . $article->id . '"><i class="fa fa-trash"></i></a> ';
            }

            return $action;
         })
         ->rawColumns(['recommended', 'publish', 'action'])
         ->toJson();
    }
}

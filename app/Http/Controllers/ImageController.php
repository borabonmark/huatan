<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Traits\CustomImageUpload;
use Illuminate\Support\Facades\Validator;

class ImageController extends Controller
{
    use CustomImageUpload;

    /**
     * Upload Image
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string 
     */
    public function upload(Request $request)
    {
        $width = $request->width;
        $height = $request->height;
        $size = $request->size * 1024;

        $validation = Validator::make($request->all(),[
            'file' => [
                'required', 
                'image',
                'max:' . $size,
                Rule::dimensions()->minWidth($width)->minHeight($height)
            ]
        ], [
            'file.required' => 'File is required.',
            'file.image' => 'File must be an image.',
            'file.max' => 'File size is too large.',
            'file.dimensions' => 'File must be atleast ' . $width . 'x' . $height .' pixels.'
        ]);

        if ($validation->fails()) {
            return response($validation->errors()->first('file'), 400);
        }

        return [
            'image' => $this->customImageUpload($request->file, 'images', 'gcs'),
            'thumbnail' => $this->customImageUpload($request->file, 'images', 'gcs', $width, $height)
        ];
    }
}

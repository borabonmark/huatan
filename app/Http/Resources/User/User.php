<?php

namespace App\Http\Resources\User;

use Illuminate\Http\Resources\Json\JsonResource;

class User extends JsonResource
{

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'firstname' => $this->getMetaByKey('firstname'),
            'lastname' => $this->getMetaByKey('lastname'),
            'avatar' => $this->getMetaByKey('avatar'),
            'image_links' => $this->photos()->pluck('value'),
            'image_id' => $this->photos()->pluck('id'),
            'online' => $this->getMetaByKey('online'),
            'city' => $this->getMetaByKey('city'),
            'age' => $this->getMetaByKey('age'),
            'gender' => $this->getMetaByKey('gender'),
            'description' => $this->getMetaByKey('description'),
            'status' => $this->getMetaByKey('status'),
            'verification' => $this->getMetaByKey('verification'),
            'height' => $this->getMetaByKey('height'),
            'weight' => $this->getMetaByKey('weight'), 
            'longitude' => $this->getMetaByKey('longitude'),
            'latitude' => $this->getMetaByKey('latitude'),
            'chat_id' => $this->getMetaByKey('chat_id'),     
        ];
    }
}

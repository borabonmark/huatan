<?php

namespace App\Http\Resources\Post;

use Illuminate\Http\Resources\Json\JsonResource;
use Carbon\Carbon;

class Post extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user_id' => $this->user_id,
            'firstname' => $this->user->firstname,
            'lastname' => $this->user->lastname,
            'avatar' => $this->user->avatar,
            'title' => $this->getMetaByKey('title'),
            'content' => $this->content,
            'image_links' => $this->getMetaByKey('photo'),
            'city' => $this->getMetaByKey('city'),
            'created_at' => $this->created_at->diffForHumans(),
        ];
    }
}

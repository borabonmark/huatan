<?php

namespace App\Http\Resources\Merchant;

use Illuminate\Http\Resources\Json\JsonResource;

class Merchant extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'email' => $this->email,
            'phone' => $this->phone,
            'category' => $this->category->name,
            'logo_link' => $this->logo,
            'address' => $this->address,
            'comment_count' => $this->totalComments->count(),
            'tags' => $this->tags->pluck('name'),
            'image_links' => $this->photos()->pluck('value'),
            'longitude' => $this->getMetaByKey('longitude'),
            'latitude' => $this->getMetaByKey('latitude'), 
            'commentable_type' => 'Merchant',
            'created_at' => $this->created_at->diffForHumans(),       
        ];
    }
}

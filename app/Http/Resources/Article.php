<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Carbon\Carbon;

class Article extends JsonResource
{


    public function toArray($request)
    {
    
        return [
            'id' => $this->id,
            'title' => $this->title,
            'content' => $this->content,
            'category' => $this->category->name,
            'image_count' =>  $this->photos()->count(),
            'comment_count' =>  $this->totalComments->count(),
            'image_links' => $this->photos()->pluck('value'),
            'thumbnails' => $this->thumbnails()->pluck('value'),
            'recommended' => $this->getMetaByKey('recommended'),
            'views' => $this->getMetaByKey('click'),
            'slug_link' => env('APP_URL').'/articlew/'.$this->id,
            'commentable_type' => 'Article',
            'author' => $this->user->firstname .' '. $this->user->lastname,
            'created_at' => $this->created_at->diffForHumans(),
        ];
    }
}

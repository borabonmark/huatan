<?php

namespace App\Http\Resources\Comment;

use Illuminate\Http\Resources\Json\JsonResource;

class Comment extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user_id' => $this->user_id,
            'firstname' => $this->user->firstname,
            'lastname' => $this->user->lastname,
            'avatar' => $this->user->avatar,
            'content' => $this->content,
            'description' => $this->getMetaByKey('description'),
            'reply' => $this->replies->count(),
            'created_at' => $this->created_at->diffForHumans(),
        ];
    }
}

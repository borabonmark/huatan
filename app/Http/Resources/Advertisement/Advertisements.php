<?php

namespace App\Http\Resources\Advertisement;

use Illuminate\Http\Resources\Json\ResourceCollection;

class Advertisements extends ResourceCollection
{

    public function toArray($request)
    {
        return parent::toArray($request);
    }
}

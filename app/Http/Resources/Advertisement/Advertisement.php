<?php

namespace App\Http\Resources\Advertisement;

use Illuminate\Http\Resources\Json\JsonResource;
use Carbon\Carbon;

class Advertisement extends JsonResource
{

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'url' => $this->url,
            'title' => $this->title,
            'material' => $this->material,
            'status' => $this->status,
            'commentable_type' => 'Advertisement',
            'created_at' => $this->created_at->diffForHumans(),               
        ];
    }
}

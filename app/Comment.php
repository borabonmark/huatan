<?php

namespace App;

use App\User;
use App\Comment;
use App\CommentMeta;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'user_id', 'parent_id', 'commentable_id', 'commentable_type', 'content'
    ];

    /**
     * Get all of the owning commentable models.
     */
    public function commentable()
    {
        return $this->morphTo();
    }

    /**
     * Get the user
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get replies
     */
    public function replies()
    {
        return $this->hasMany(Comment::class, 'parent_id');
    }

    /**
     * Get all replies
     */
    public function allReplies()
    {
        return $this->hasMany(Comment::class, 'commentable_id');
    }

    /**
     * Get meta
     */
    public function meta()
    {
        return $this->hasMany(CommentMeta::class);
    }

    /**
     * Get specific meta data value
     */
    public function getMetaByKey($key)
    {
        return $this->meta->where('key', $key)->pluck('value')->first();
    }
}

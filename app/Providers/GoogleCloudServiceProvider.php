<?php

namespace App\Providers;

use Storage;
use League\Flysystem\Filesystem;
use Google\Cloud\Storage\StorageClient;
use Illuminate\Support\ServiceProvider;
use Superbalist\Flysystem\GoogleStorage\GoogleStorageAdapter;

class GoogleCloudServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Storage::extend('gcs', function ($app, $config) {
            $client = new StorageClient($config);
            return new Filesystem(new GoogleStorageAdapter($client, $client->bucket($config['bucket'])));
        });
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}

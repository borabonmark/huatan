<?php

namespace App\Providers;

use Laravel\Passport\Passport;

use App\User;
use App\Article;
use App\Merchant;
use App\Advertisement;
use App\Policies\UserPolicy;
use App\Policies\ArticlePolicy;
use App\Policies\MerchantPolicy;
use App\Policies\AdvertisementPolicy;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        User::class => UserPolicy::class,
        Article::class => ArticlePolicy::class,
        Merchant::class => MerchantPolicy::class,
        Advertisement::class => AdvertisementPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Passport::routes();
    }
}

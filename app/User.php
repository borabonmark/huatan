<?php

namespace App;

use Laravel\Passport\HasApiTokens;

use App\Module;
use App\Article;
use App\Report;
use App\Bookmark;
use App\UserMeta;
use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;


    public function findForPassport($username) {
        return $this->orWhere('email', $username)->orWhere('phone', $username)->first();
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'email', 'phone', 'password', 'role', 'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    
    /**
     * The attributes that are coming from meta table
     *
     * @var array
     */
    protected $appends  = [
         'firstname', 'lastname', 'avatar'
    ];

    /**
     * List of status
     */
    const STATUS_ACTIVE = 'active';
    const STATUS_INACTIVE = 'inactive';

    public static $statuses = [
        self::STATUS_ACTIVE,
        self::STATUS_INACTIVE
    ];

    /**
     * List of roles
     */
    const ROLE_SUPER_ADMIN = 'super admin';
    const ROLE_ADMIN = 'admin';
    const ROLE_USER = 'user';

    public static $roles = [
        self::ROLE_SUPER_ADMIN,
        self::ROLE_ADMIN,
        self::ROLE_USER
    ];

    /**
     * Get modules
     */
    public function modules()
    {
        return $this->belongsToMany(Module::class)
                    ->withPivot('action')
                    ->withTimestamps();
    }

    /**
     * Get access per module
     *
     * @param App\Module
     */
    public function access(Module $module)
    {
        return $this->modules->where('id', $module->id)
                             ->pluck('pivot.action')
                             ->toArray();
    }

    /**
     * Check role
     *
     * @return boolean 
     */
    public function hasRole($role)
    {
        return $this->role == $role;
    }

    /**
     * Get all created article
     */
    public function articles()
    {
        return $this->hasMany(Article::class);
    }

    /**
     * Get meta
     */
    public function meta()
    {
        return $this->hasMany(UserMeta::class);
    }

    /**
     * Get specific meta data value
     */
    public function getMetaByKey($key)
    {
        return $this->meta->where('key', $key)->pluck('value')->first();
    }

    /**
     * Get photos
     */
    public function photos($limit = 0)
    {
        if (1 == $limit) {
            return $this->meta()->where('key', 'photo')->first();
        } elseif ($limit > 1) {
            return $this->meta()->where('key', 'photo')->get()->take($limit);
        } else {
            return $this->meta()->where('key', 'photo')->get();
        }
    }

     /**
     * Get all bookmarks
     */
    public function bookmarks()
    {
        return $this->hasMany(Bookmark::class);
    }

     /**
     * Get all reports
     */
    public function reports()
    {
        return $this->hasMany(Report::class);
    }

    /**
     * Get firstname
     *
     */
    public function getFirstnameAttribute()
    {
        return $this->getMetaByKey('firstname');
    }

    /**
     * Get firstname
     *
     */
    public function getLastnameAttribute()
    {
        return $this->getMetaByKey('lastname');
    }

    /**
     * Get firstname
     *
     */
    public function getAvatarAttribute()
    {
        return $this->getMetaByKey('avatar');
    }
}

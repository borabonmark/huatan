<?php

namespace App\Traits;

use Image;
use Illuminate\Support\Facades\Storage;

trait CustomImageUpload
{
	/**
     * Custom image upload.
     *
     * @param  \Illuminate\Http\UploadedFile  $file
     * @param  int $width
     * @param  int $height
     * @param  string $folder
     * @param  string $disk
     *
     * @return string 
     */
    public function customImageUpload($file, $folder = NULL, $disk = 'public', $width = 0, $height = 0) {
        // if height is empty set value same with width
        $height = $height ? $height : $width; 

        // set file name , append dimension if width and height exists
        $filename = ($width && $height) ?  
                    $width . 'x' .  $height . $file->hashName() :
                    $file->hashName();

        // set upload path
        $path = $folder ? $folder . '/' . $filename : $filename;


        $image = Image::make($file);

        // if has width resize image
        if ($width) {
            $image->fit((int)$width, (int)$height, function ($constraint) {
                $constraint->aspectRatio();
            });
        }

        // store image
        Storage::disk($disk)->put($path, (string)$image->encode());

        // get stored image full url
        return Storage::disk($disk)->url($path);
    }
}

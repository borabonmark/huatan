<?php

namespace App;

use App\Article;
use Illuminate\Database\Eloquent\Model;

class ArticleMeta extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'article_meta';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'article_id', 'name', 'key', 'value'
    ];

    /**
     * Get the article
     */
    public function article()
    {
        return $this->belongsTo(Article::class);
    }
}
